<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ConnexionBucheronController extends AbstractController
{
    /*
    * TODO
    * @return Responses
    */
    #[Route('/connexion-bucheron', name: 'app_connexion-bucheron')]
    public function index(): Response
    {
        // utilisation du fichier de vue connexion_bucheron/index.html.twig
        return $this->render('connexion_bucheron/index.html.twig', [
            'controller_name' => 'ConnexionBucheronController',
        ]);
    }

    /**
    * 
    * 
    * 
    */
    public function loginer(SessionInterface $session, Request $request, $type)
    {
        $name = $request->request->get('name');
        $email = $request->request->get('email');
    
        $user = 1;// Vérifiez si les informations d'identification sont valides pour "bucheron" ou "propriétaire" en fonction de $type
    
        if (!$user) {
            return $this->render('login/error.html.twig');
        }
    
        $session->set('user', $user);
    
        return $this->redirectToRoute('home');
    }
}
