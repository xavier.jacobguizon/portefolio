<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends AbstractController
{
    /*
    * TODO
    * @return Responses
    */
    #[Route('/menu', name: 'app_menu')]
    public function index(): Response
    {
        return $this->redirectToRoute('app_acceuil');
        
        

        // utilisation du fichier de vue menu/index.html.twig
        return $this->render('menu/index.html.twig', [
            'controller_name' => 'MenuController',
        ]);
    }
}
