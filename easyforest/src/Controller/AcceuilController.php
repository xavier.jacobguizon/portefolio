<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AcceuilController extends AbstractController
{
    /*
    * TODO
    * @return Responses
    */
    #[Route('/acceuil', name: 'app_acceuil')]
    public function index(SessionInterface $session): Response
    {
        // Vérification de l'état de connexion de l'utilisateur
        // if (!$session->get('user')) {
        //     return $this->redirectToRoute('app_connexion-proprietaire');
        // }

        return $this->render('acceuil/index_client.html.twig', []);

        // // utilisation du fichier de vue acceuil/index.html.twig
        return $this->render('acceuil/index_bucheron.html.twig', [
            'controller_name' => 'AcceuilController',
        ]);
    }
}
