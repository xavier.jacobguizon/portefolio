package com.mycompany.app;

import model.CaseTuile;
import model.Partie;
import jeu.GestionJeux;
import jeu.Grille;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PartieTests {

    private Partie partieFusion, partieDeplacementStandardDroite,partieDeplacementStandardGauche,partieDeplacementImpossible;
    private Partie partieIntraGrilleFusion;

    private Partie complexepartieFusion;//, complexepartieDeplacementStandardDroite,complexepartieDeplacementStandardGauche,complexepartieDeplacementImpossible;
    private Partie grille2048, grilleAucunDeplacement;

    @Before
    public void setUp() {
        CaseTuile t2 = new CaseTuile(0,0,8);

        CaseTuile t1 = new CaseTuile(0,0);
        //CaseTuile t3 = new CaseTuile(0,1);
        CaseTuile t4 = new CaseTuile(1,0);
        CaseTuile t5 = new CaseTuile(1,1);
        CaseTuile t6 = new CaseTuile(1,1);
        CaseTuile t7 = new CaseTuile(2,0);
        CaseTuile t8 = new CaseTuile(2,0);
        CaseTuile t9 = new CaseTuile(1,1);
        CaseTuile t10 = new CaseTuile(0,0);

        /*
        INTRAGRILLE
         */
        this.partieIntraGrilleFusion = new Partie(3);
        this.partieIntraGrilleFusion.getGrilles().get(0).ajouterTuile(t1);
        this.partieIntraGrilleFusion.getGrilles().get(0).ajouterTuile(t4);


        /*
        INTERGRILLE
         */
        //Setup fusion
        this.partieFusion = new Partie(3);


        this.partieFusion.getGrilles().get(0).ajouterTuile(t1);
        this.partieFusion.getGrilles().get(2).ajouterTuile(t1);


        //Setup déplacement standard vers droite
        this.partieDeplacementStandardDroite = new Partie(3);
        this.partieDeplacementStandardDroite.getGrilles().get(0).ajouterTuile(t1);

        //Setup déplacement standard vers gauche
        this.partieDeplacementStandardGauche = new Partie(3);
        this.partieDeplacementStandardGauche.getGrilles().get(2).ajouterTuile(t1);

        //Setup déplacement impossible
        this.partieDeplacementImpossible = new Partie(3);
        this.partieDeplacementImpossible.getGrilles().get(0).ajouterTuile(t1);
        this.partieDeplacementImpossible.getGrilles().get(2).ajouterTuile(t2);

        //setup complexe



        //Setup fusion
        this.complexepartieFusion = new Partie(3);


        this.complexepartieFusion.getGrilles().get(0).ajouterTuile(t1);
        this.complexepartieFusion.getGrilles().get(0).ajouterTuile(t8);
        this.complexepartieFusion.getGrilles().get(0).ajouterTuile(t5);

        this.complexepartieFusion.getGrilles().get(1).ajouterTuile(t6);

        this.complexepartieFusion.getGrilles().get(2).ajouterTuile(t10);
        this.complexepartieFusion.getGrilles().get(2).ajouterTuile(t7);
        this.complexepartieFusion.getGrilles().get(2).ajouterTuile(t9);


        //Setup déplacement standard vers droite
        this.partieDeplacementStandardDroite = new Partie(3);
        this.partieDeplacementStandardDroite.getGrilles().get(0).ajouterTuile(t1);

        //Setup déplacement standard vers gauche
        this.partieDeplacementStandardGauche = new Partie(3);
        this.partieDeplacementStandardGauche.getGrilles().get(2).ajouterTuile(t1);

        //Setup déplacement impossible
        this.partieDeplacementImpossible = new Partie(3);
        this.partieDeplacementImpossible.getGrilles().get(0).ajouterTuile(t1);
        this.partieDeplacementImpossible.getGrilles().get(2).ajouterTuile(t2);

        //Setup 2048 présent
        this.grille2048 = new Partie(3);
        this.grille2048.getGrilles().get(0).ajouterTuile(new CaseTuile(0,0,2048));

        //Setup aucun déplacement
        this.grilleAucunDeplacement = new Partie(3);
         for(int i = 0; i < grilleAucunDeplacement.getGrilles().size();i++){
            this.grilleAucunDeplacement.getGrilles().get(i).remplirLaGrille();
         }



    }

    @Test
    public void deplacementInterGrilleStandard(){
        System.out.println("AVANT______");
        System.out.println(this.partieDeplacementStandardDroite);

        this.partieDeplacementStandardDroite.deplacerCasesInterGrille(GestionJeux.INTERGRIDDROITE);
        System.out.println("APRES______");
        System.out.println(this.partieDeplacementStandardDroite);
        Grille grille1 = this.partieDeplacementStandardDroite.getGrilles().get(0);
        Grille grille2 = this.partieDeplacementStandardDroite.getGrilles().get(2);
        assertFalse(grille1.isCaseAtCoord(0,0));
        assertTrue(grille2.isCaseAtCoord(0,0));
        assertEquals(2,((CaseTuile)grille2.getCases()[0][0]).getNombre());

    }

    @Test
    public void deplacementInterGrilleFusion(){
        System.out.println("FUSION INTERGRILLE");
        System.out.println("AVANT____");
        System.out.println(this.partieFusion);
        this.partieFusion.deplacerCasesInterGrille(GestionJeux.INTERGRIDDROITE);
        System.out.println("APRES____");
        System.out.println(this.partieFusion);

        Grille grille1 = this.partieFusion.getGrilles().get(0);
        Grille grille2 = this.partieFusion.getGrilles().get(2);

        assertFalse(grille1.isCaseAtCoord(0,0));
        assertTrue(grille2.isCaseAtCoord(0,0));
        assertEquals(4,((CaseTuile)grille2.getCases()[0][0]).getNombre());
    }

    @Test
    public void deplacementIntergrilleImpossible(){
        System.out.println("DEPLACEMENT IMPOSSIBLE INTERGRILLE");
        this.partieDeplacementImpossible.getGrilles().get(1).getCases()[0][0] = new CaseTuile(0,0,4);
        System.out.println("AVANT____");
        System.out.println(this.partieDeplacementImpossible);
        this.partieDeplacementImpossible.deplacerCasesInterGrille(GestionJeux.INTERGRIDDROITE);
        System.out.println("APRES____");
        System.out.println(this.partieDeplacementImpossible);

        Grille grille1 = this.partieDeplacementImpossible.getGrilles().get(0);
        Grille grille2 = this.partieDeplacementImpossible.getGrilles().get(2);

        assertTrue(grille1.isCaseAtCoord(0,0));
        assertTrue(grille2.isCaseAtCoord(0,0));
        assertEquals(8,((CaseTuile)grille2.getCases()[0][0]).getNombre());
        assertEquals(2,((CaseTuile)grille1.getCases()[0][0]).getNombre());
    }

    @Test
    public void fusionComplexeIntergrilleDroit(){
        System.out.println("FUSION COMPLEXE INTERGRILLE");
        System.out.println("AVANT___");
        System.out.println(this.complexepartieFusion);
        this.complexepartieFusion.deplacerCasesInterGrille(GestionJeux.INTERGRIDDROITE);
        System.out.println("APRES____");
        System.out.println(this.complexepartieFusion);

        Grille grille1 = this.complexepartieFusion.getGrilles().get(0);
        Grille grille2 = this.complexepartieFusion.getGrilles().get(1);
        Grille grille3 = this.complexepartieFusion.getGrilles().get(2);


        //Test de la présence de cases
        assertTrue(grille3.isCaseAtCoord(0,0));
        assertTrue(grille3.isCaseAtCoord(0,2));
        assertTrue(grille3.isCaseAtCoord(1,1));

        assertTrue(grille2.isCaseAtCoord(1,1));

        assertFalse(grille1.isCaseAtCoord(1,1));
        assertFalse(grille1.isCaseAtCoord(0,0));
        assertFalse(grille1.isCaseAtCoord(0,2));

        //Test de la valeur des cases
        assertEquals(4,((CaseTuile)grille3.getCases()[0][0]).getNombre());
        assertEquals(4,((CaseTuile)grille3.getCases()[0][2]).getNombre());
        assertEquals(4,((CaseTuile)grille3.getCases()[1][1]).getNombre());
        assertEquals(2,((CaseTuile)grille2.getCases()[1][1]).getNombre());
    }

    @Test
    public void fusionComplexeIntergrilleGauche(){
        System.out.println("FUSION COMPLEXE INTERGRILLE");
        System.out.println("AVANT___");
        System.out.println(this.complexepartieFusion);
        this.complexepartieFusion.deplacerCasesInterGrille(GestionJeux.INTERGRIDGAUCHE);
        System.out.println("APRES____");
        System.out.println(this.complexepartieFusion);

        Grille grille1 = this.complexepartieFusion.getGrilles().get(0);
        Grille grille2 = this.complexepartieFusion.getGrilles().get(1);
        Grille grille3 = this.complexepartieFusion.getGrilles().get(2);


        //Test de la présence de cases
        assertTrue(grille1.isCaseAtCoord(0,0));
        assertTrue(grille1.isCaseAtCoord(0,2));
        assertTrue(grille1.isCaseAtCoord(1,1));

        assertTrue(grille2.isCaseAtCoord(1,1));

        assertFalse(grille3.isCaseAtCoord(1,1));
        assertFalse(grille3.isCaseAtCoord(0,0));
        assertFalse(grille3.isCaseAtCoord(0,2));

        //Test de la valeur des cases
        assertEquals(4,((CaseTuile)grille1.getCases()[0][0]).getNombre());
        assertEquals(4,((CaseTuile)grille1.getCases()[0][2]).getNombre());
        assertEquals(4,((CaseTuile)grille1.getCases()[1][1]).getNombre());
        assertEquals(2,((CaseTuile)grille2.getCases()[1][1]).getNombre());
    }

    @Test
    public void verificationScoreDeplacementReussiIntraGrille(){
        System.out.println("-La partie");
        System.out.println(this.partieIntraGrilleFusion);
        this.partieIntraGrilleFusion.deplacerCasesIntraGrille(GestionJeux.DROITE);
        assertEquals(4,this.partieIntraGrilleFusion.getJoueur().getScore());
    }

    @Test
    public void verificationScoreDeplacementReussiInterGrille(){
        System.out.println("-La partie");
        System.out.println(this.partieFusion);
        this.partieFusion.deplacerCasesInterGrille(GestionJeux.INTERGRIDDROITE);
        System.out.println(this.partieFusion);
        assertEquals(4,this.partieFusion.getJoueur().getScore());
    }

    @Test
    public void verifier2048(){
        boolean test = this.grille2048.getGrilles().get(0).verifier2048();
        assertTrue("Devrait être vrai : 2048 présent",test);
    }

    @Test
    public void partieTermine(){
        boolean test = this.grille2048.verifierSiTermine();
        assertTrue("Devrait être vrai : la partie est terminé",test);
    }

    @Test
    public void partieTermineAucunDeplacement(){
        boolean testTermine = this.grilleAucunDeplacement.verifierSiTermine();
        boolean testDeplacementIntragrille = this.grilleAucunDeplacement.getGrilles().get(0).verifierSiCaseVide();
        boolean testfusion = this.grilleAucunDeplacement.getGrilles().get(0).verifierSiFusionPossible();
        boolean testIntergrille = this.grilleAucunDeplacement.verifierSiDeplacementInterGrillePossible();
        System.out.println(this.grilleAucunDeplacement);
        assertFalse("Devrait être faux : aucune case vide",testDeplacementIntragrille);
        assertFalse("Devrait être faux : aucune fusion possible",testfusion);
        assertFalse("Devrait être faux : aucun déplacement intergrille possible",testIntergrille);
        assertTrue("Devrait être vrai : la partie est terminé car aucun déplacement n'est possible",testTermine);

    }
}
