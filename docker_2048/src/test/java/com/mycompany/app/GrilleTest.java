package com.mycompany.app;

import model.CaseTuile;
import jeu.GestionJeux;
import jeu.Grille;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GrilleTest {
    private Grille grilleCaseMid, grilleCaseFusion, grilleCaseFusion2;

    @Before
    public void setUp() {
        /*
        Créer une grille de test 3*3
            000
            020
            000

         Créer une grille de test 3*3
            022
            000
            000
         */
        this.grilleCaseMid = new Grille(3);
        this.grilleCaseFusion = new Grille(3);
        this.grilleCaseFusion2 = new Grille(3);
        grilleCaseMid.getCases()[1][1] = new CaseTuile(1, 1);
        grilleCaseFusion.getCases()[0][1] = new CaseTuile(1, 0);
        grilleCaseFusion.getCases()[0][2] = new CaseTuile(2, 0);
        CaseTuile tuile1 = new CaseTuile(1, 0);
        CaseTuile tuile2 = new CaseTuile(2, 0);
        tuile1.setNombre(4);
        grilleCaseFusion2.getCases()[0][1] = tuile1;
        grilleCaseFusion2.getCases()[0][2] = tuile2;
        System.out.println("Grille Mid :");
        System.out.println(grilleCaseMid);

        System.out.println("Grille Fusion");
        System.out.println(grilleCaseFusion);
    }

    @Test
    public void testDetectionCaseTuileVrai() {
        assertTrue(this.grilleCaseMid.isCaseAtCoord(1, 1));
    }

    @Test
    public void testDetectionCaseTuileFaux() {
        assertFalse(this.grilleCaseMid.isCaseAtCoord(0, 2));
    }

    @Test
    public void deplacerGauche() {
        this.grilleCaseMid.deplacerCases(3);
        System.out.println("Apres un déplacement à gauche :");
        System.out.println(this.grilleCaseMid);
        assertTrue(this.grilleCaseMid.isCaseAtCoord(1, 0));
    }

    @Test
    public void deplacerDroite() {
        this.grilleCaseMid.deplacerCases(4);
        System.out.println("Apres un déplacement à droite :");
        System.out.println(this.grilleCaseMid);
        assertTrue(this.grilleCaseMid.isCaseAtCoord(1, 2));
    }

    @Test
    public void deplacerHaut() {
        this.grilleCaseMid.deplacerCases(1);
        System.out.println("Apres un déplacement en haut :");
        System.out.println(this.grilleCaseMid);
        assertTrue(this.grilleCaseMid.isCaseAtCoord(0, 1));
    }

    @Test
    public void deplacerBas() {
        this.grilleCaseMid.deplacerCases(2);
        System.out.println("Apres un déplacement en bas :");
        System.out.println(this.grilleCaseMid);
        assertTrue(this.grilleCaseMid.isCaseAtCoord(2, 1));
    }

    @Test
    public void deplacerHorsGrille() {
    }

    @Test
    public void fusionnerCase() {
        this.grilleCaseFusion.deplacerCases(4);
        assertFalse(this.grilleCaseFusion.isCaseAtCoord(0, 1));
        assertTrue(this.grilleCaseFusion.isCaseAtCoord(0, 2));
        assertEquals(4, ((CaseTuile) this.grilleCaseFusion.getCases()[0][2]).getNombre());
    }

    @Test
    public void fusionnerCaseImpossible() {
        this.grilleCaseFusion2.deplacerCases(4);
        System.out.println("Grille fusion 2 après déplacement");
        System.out.println(this.grilleCaseFusion2);
        assertTrue(this.grilleCaseFusion2.isCaseAtCoord(0, 1));
        assertTrue(this.grilleCaseFusion2.isCaseAtCoord(0, 2));
        assertEquals(2, ((CaseTuile) this.grilleCaseFusion2.getCases()[0][2]).getNombre());
        assertEquals(4, ((CaseTuile) this.grilleCaseFusion2.getCases()[0][1]).getNombre());
    }

    @Test
    public void deplacementMultiple(){
        System.out.println("__AVANT");
        System.out.println(this.grilleCaseFusion);
        this.grilleCaseFusion.deplacerCases(GestionJeux.BAS);
        this.grilleCaseFusion.ajouterTuile(new CaseTuile(0,2));
        System.out.println("__APRES");
        System.out.println(this.grilleCaseFusion);
        this.grilleCaseFusion.deplacerCases(GestionJeux.HAUT);
        this.grilleCaseFusion.ajouterTuile(new CaseTuile(0,2));
        System.out.println("__APRES");
        System.out.println(this.grilleCaseFusion);
        this.grilleCaseFusion.deplacerCases(GestionJeux.DROITE);
        System.out.println("__APRES");
        System.out.println(this.grilleCaseFusion);
    }

}