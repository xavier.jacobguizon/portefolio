package bdd;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Cette classe gére la récupération et la mise en place du tableau des scores
 */
public class ScoreBoard {
    /**
     * Liste des joueurs présent dans le tableau des scores
     */
    private ArrayList<JoueurDB> joueurs;

    /**
     * Créer une instance de la classe Scoreboard
     */
    public ScoreBoard(){
        this.joueurs = new ArrayList<JoueurDB>();
    }

    /**
     * Permet de générer un tableau des scores
     * @return
     */
    public ArrayList<JoueurDB> getScoreBoard(){
        this.joueurs = JoueurDB.getAllPlayers();
        Collections.sort(this.joueurs, (o1, o2) -> {
            JoueurDB j1 = o1;
            JoueurDB j2 = o2;
            int res = 0;
            if(j1.getScore() > ( j2).getScore()){
                res = 1;
            }else{
                res = -1;
            }
            return res ;
        });
        return this.joueurs;
    }
}
