package bdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Cette classe représente le tuple d'un serveur dans la base de données
 */
public class Serveurs {
    /**
     * Le nom, le status et l'ip du host du serveur
     */
    private String nom, status, host;

    /**
     * Le nombre de joueur du serveur
     */
    private int nbJoueur;

    /**
     * Le port du serveur
     */
    private int port;

    /**
     * L'ID du serveur dans la base de données
     */
    private int ID;

    /**
     * Le mode de jeu du serveur
     */
    private String gamemode;

    /**
     * Permet la création d'un tuple pour la base de données
     * @param nom
     * @param status
     * @param nbJoueur
     * @param host
     * @param port
     * @param gamemode
     */
    public Serveurs(String nom, String status, int nbJoueur, String host, int port, String gamemode){
        this.nbJoueur = nbJoueur;
        this.nom = nom;
        this.status = status;
        this.ID = -1;
        this.host = host;
        this.port = port;
        this.gamemode = gamemode;
    }

    /**
     * Permet de créer un serveur récupéré depuis la base de données
     * @param nom
     * @param status
     * @param nbJoueur
     * @param host
     * @param port
     * @param id
     * @param gamemode
     */
    public Serveurs(String nom, String status, int nbJoueur, String host, int port, int id, String gamemode){
        this.nbJoueur = nbJoueur;
        this.nom = nom;
        this.status = status;
        this.ID = id;
        this.gamemode = gamemode;
        this.host = host;
        this.port = port;
    }

    /**
     * Permet de Sauvegarder ou mettre à jour un tuple
     */
    public void save() {

        if(this.ID==-1) {
            this.saveNew();
        }
        else {
            this.update();
        }
    }

    /**
     * permet de sauevagrder un nouveau tuple dans la base de données
     */
    private void saveNew() {
        try {
            Connection connect=DBConnect.getInstance();
            String SQLPrep0 = "INSERT INTO Serveurs (`Nom`, `NbJoueurs`, `Status`, `Host`, `Port`, `Gamemode`) VALUES" +
                    "('"+this.nom+"', '"+this.nbJoueur+"', '"+this.status+"', '"+this.host+"', '"+this.port+"', '"+this.gamemode+"')";
            PreparedStatement prep0 = connect.prepareStatement(SQLPrep0);
            prep0.execute();
            String SQLPrep = "SELECT * FROM Serveurs WHERE Host ='"+this.host+"'";
            PreparedStatement prep1 = connect.prepareStatement(SQLPrep);
            prep1.execute();
            ResultSet rs = prep1.getResultSet();
            int idres = -1;
            while (rs.next()) {
                idres = rs.getInt("ID");
            }
            this.ID=idres;
        }
        catch(SQLException e) {
            System.out.println(e.getMessage()+"new "+e.getErrorCode()+e.toString());
        }
    }

    /**
     * Permet de mettre à jour un tuple dans la base de données
     */
    private void update() {
        try {
            Connection connect=DBConnect.getInstance();
            String SQLPrep0 = "UPDATE Serveurs " +
                    "SET Nom = '"+this.nom+"', NbJoueurs = '"+this.nbJoueur+"', Status ='"+this.status+"', Host ='"+this.host+"', Port ='"+this.port+"', Gamemode ='"+this.gamemode+"'"+
                    "WHERE ID ='"+this.ID+"';";
            PreparedStatement prep0 = connect.prepareStatement(SQLPrep0);
            prep0.execute();
        }
        catch(SQLException e) {
            System.out.println(e.getMessage()+"update "+e.getErrorCode()+e.toString());
        }
    }


    /**
     * Permet de récupérer la liste des serveurs acceptant des joueurs
     * @return
     */
    public static ArrayList<Serveurs> getServeursList(){
        ArrayList<Serveurs> res = new ArrayList<Serveurs>();
        try{
            Connection connect = DBConnect.getInstance();
            PreparedStatement requete = connect.prepareStatement("SELECT * FROM Serveurs");
            requete.execute();
            ResultSet rs = requete.getResultSet();
            while(rs.next()){
                String status = rs.getString("Status");
                if(status.equals("WAITING")){
                    res.add(new Serveurs(rs.getString("Nom"),status,rs.getInt("NbJoueurs"), rs.getString("Host"),rs.getInt("Port"), rs.getInt("ID"), rs.getString("Gamemode")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Permet de supprimer un serveur
     */
    public void delete(){
        try {
            Connection connect=DBConnect.getInstance();
            String SQLPrep0 = "DELETE FROM Serveurs WHERE ID ='"+this.ID+"';";
            PreparedStatement prep0 = connect.prepareStatement(SQLPrep0);
            prep0.execute();
            this.ID=-1;
        }
        catch(SQLException e) {
            System.out.println(e.getMessage()+" Delete "+e.getErrorCode()+e.toString());
        }
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNbJoueur() {
        return nbJoueur;
    }

    public void setNbJoueur(int nbJoueur) {
        this.nbJoueur = nbJoueur;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getGamemode() {
        return gamemode;
    }

    public void setGamemode(String gamemode) {
        this.gamemode = gamemode;
    }
}
