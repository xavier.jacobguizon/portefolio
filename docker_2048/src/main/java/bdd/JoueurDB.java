package bdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Cette classe représente le tuple d'un Joueur dans la base de données
 */
public class JoueurDB {
    /**
     * Pseudo du joueur
     */
    private String pseudo;
    /**
     * Score du joueur
     */
    private int score;

    private int position;
    /**
     * ID du joueur dans la Base de données
     */
    private int idJoueur;

    /**
     * Type du jeu dont provient le score du joueur
     */
    private String typeJeu;

    /**
     * Permet la création d'un tuple d'un joueur
     * @param pseudo
     * @param score
     * @param typeJeu
     */
    public JoueurDB(String pseudo, int score, String typeJeu){
        this.pseudo = pseudo;
        this.score = score;
        this.position = -1;
        this.idJoueur = -1;
        this.typeJeu = typeJeu;
    }

    /**
     * Permet derécupérer tout les joueurs
     * @return ArrayList<JoueurDB>
     */
    public static ArrayList<JoueurDB> getAllPlayers(){
        ArrayList<JoueurDB> res = new ArrayList<JoueurDB>();
        try{
            Connection connect = DBConnect.getInstance();
            PreparedStatement requete = connect.prepareStatement("SELECT * FROM Joueurs");
            requete.execute();
            ResultSet rs = requete.getResultSet();
            while(rs.next()){
                res.add(new JoueurDB(rs.getString("Pseudo"),rs.getInt("Score"),rs.getString("TypeJeu")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Permet de sauvegarder un joueur dans la base de ddonnées
     */
    public void save() {
        try {
            Connection connect=DBConnect.getInstance();
            String SQLPrep0 = "INSERT INTO Joueurs (`Pseudo`, `Score`, `TypeJeu`) VALUES" +
                    "('"+this.pseudo+"', '"+this.score+"', '"+this.typeJeu+"')";
            PreparedStatement prep0 = connect.prepareStatement(SQLPrep0);
            prep0.execute();
        }
        catch(SQLException e) {
            System.out.println(e.getMessage()+"new "+e.getErrorCode()+e.toString());
        }
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getIdJoueur() {
        return idJoueur;
    }

    public void setIdJoueur(int idJoueur) {
        this.idJoueur = idJoueur;
    }

    public String getTypeJeu() {
        return typeJeu;
    }

    public void setTypeJeu(String typeJeu) {
        this.typeJeu = typeJeu;
    }
}
