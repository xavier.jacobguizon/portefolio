package bdd;



import com.mycompany.app.App;

import java.io.*;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Classe gérant la connexion à la base de données
 */
public class DBConnect {

    public static Connection connect;
    private String serverName;
    private int port;
    private String dbName;

    private DBConnect(){
        try{
            this.readInfoConnexion();
            Properties connectionsProps = new Properties();
            connectionsProps.put("user","ginklaw_lucas");
            connectionsProps.put("password","SciencesCoProjet");
            String urlDB = "jdbc:mysql://"+serverName+":"+port+"/"+dbName;
            connect = DriverManager.getConnection(urlDB, connectionsProps);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static synchronized Connection getInstance(){
        if(connect == null){
            new DBConnect();
        }
        return connect;
    }


    /**
     * permet de lire les informations de conneixon depuis le fichier config
     */
    private void readInfoConnexion(){
        BufferedReader read = null;
        try {
            if(System.getProperty("os.name").toLowerCase().equals("linux")){
                read = new BufferedReader(new FileReader(new File(
                    App.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent()+"/cfg/config.txt"));

            }else{
                read = new BufferedReader(new FileReader(new File(
                    App.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent()+"\\cfg\\config.txt"));
            }
            this.serverName = read.readLine().split(":")[1];
            this.port = Integer.parseInt(read.readLine().split(":")[1]);
            this.dbName = read.readLine().split(":")[1];
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
