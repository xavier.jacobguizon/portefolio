package model;

/**
 * Représente une case de type vide
 */
public class CaseVide extends Case {

    public CaseVide(int x, int y) {
        super(x, y);
    }
}
