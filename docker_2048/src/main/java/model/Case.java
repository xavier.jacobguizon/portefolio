package model;

import java.io.Serializable;

/**
 * Design Pattern Strategy
 * Représente une case
 */
public abstract class Case implements Serializable{
    /*
        Les coordonnées possible sont comprises entre 0 et 2 pour les deux axes
        du fait que la grille est une 3*3
         */
    /**
     * Coordonnée x de la case
     */
    private int x;
    /**
     * Coordonnée y de la case
     */
    private int y;

    /**
     * Permet de créer une isntance d'une case
     * @param x
     * @param y
     */
    public Case(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * Permet de définir les coordonnées d'une case
     * @param newX
     * @param newY
     */
    public void setCoord(int newX, int newY){
        this.x = newX;
        this.y = newY;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
