package model;

import controller.ControlleurDeplacementJoueur;
import controller.DeplacementStrategy;
import view.AffichageInterface;
import jeu.GestionJeux;
import jeu.TypeJeuSolo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Représente un jeu en mode graphique
 */
public class JeuGraphique extends Jeu {

    private static JeuGraphique jeuGraphique;

    private JeuGraphique(){
        super(AffichageInterface.getInstance(), new ControlleurDeplacementJoueur());
    }


    /**
     * Permet de récupérer l'instance du JeuGraphique
     * @return
     */
    public static synchronized JeuGraphique getJeuGraphique(){
        if(jeuGraphique == null){
            jeuGraphique = new JeuGraphique();
        }
        return jeuGraphique;
    }

    /**
     * Permet de charger une partie uniquement en solo
     */
    public void chargerPartie(DeplacementStrategy ds){
    	
    	
        try {
            final FileInputStream fichierIn = new FileInputStream("save.ser");
            try (ObjectInputStream ois = new ObjectInputStream(fichierIn)) {
                this.setGestionJeux((GestionJeux) ois.readObject());
            }
            System.out.println("Chargement de la partie");
        } catch (final IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } 
        
        this.getGestionJeux().getPartie().setCharge(true);
        
        this.getGraphiqueStrategy().initGraphics(this.getGestionJeux().getPartie());
        
        
        this.setDeplacementStrategy(ds);
        this.getDeplacementStrategy().initModele(this.getGestionJeux());
        
        this.setTypeJeu(new TypeJeuSolo(this.getDeplacementStrategy(),this.getGraphiqueStrategy(),this.getGestionJeux()));
        try {
            lancerJeu();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
