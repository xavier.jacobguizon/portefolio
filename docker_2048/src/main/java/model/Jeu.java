package model;

import bdd.JoueurDB;
import view.GraphiqueStrategy;

import controller.DeplacementStrategy;
import jeu.GestionJeux;
import jeu.Menu;
import jeu.TypeJeu;


/**
 * 
 * @author blondin
 * 
 * Class qui va definir le Jeu
 * Créer un style graphique et lance le jeu
 * 
 */
public abstract class Jeu{
    /**
     * Type du jeu
     */
    private TypeJeu typeJeu;
    /**
     * Permet la navigation entre les menu
     */
    private Menu menu;
    /**
     * Type graphique du jeu
     */
    private GraphiqueStrategy graphiqueStrategy;
    /**
     * Type de déplacement du jeu
     */
    private DeplacementStrategy deplacementStrategy;
    /**
     * Gestion du jeu
     */
    private GestionJeux gestionJeux;
    private int theme = 1; //Theme du jeu
    
    
    //attribut pour le choix multijoueur
    private boolean host;
    /**
     * Permet de créer une instance du jeu
     * @param graphiqueStrategy
     * @param deplacementStrategy
     */
    public Jeu(GraphiqueStrategy graphiqueStrategy, DeplacementStrategy deplacementStrategy){
        this.graphiqueStrategy = graphiqueStrategy;
        this.deplacementStrategy = deplacementStrategy;
        this.menu = new Menu(graphiqueStrategy);
        this.gestionJeux = new GestionJeux();
    }


    /**
     * Permet de changer de menu dans le jeu
     * @param typeMenu
     */
    public void changerDeMenu(int typeMenu){
        try {
            this.menu.accederAuMenu(typeMenu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * Permet de lancer le jeu
     */
    public void lancerJeu() throws Exception {
        if(getTypeJeu() != null){
            this.getTypeJeu().startTimer();
        }else{
            throw new Exception("Le type de jeu n'a pas été défini !");
        }
    }

    /**
     * Permet de terminer le jeu
     */
    public void terminerLeJeu(){
        this.getTypeJeu().stopTimer();
        /*
        Envoie du score a la BDD
         */
        JoueurDB joueurDB = new JoueurDB(this.getGestionJeux().getPartie().getJoueur().getPseudo(),this.getGestionJeux().getPartie().getJoueur().getScore(),this.getGestionJeux().getPartie().getGamemode());
        joueurDB.save();
        this.changerDeMenu(Menu.MENU_PRINCIPAL);

    }


    public TypeJeu getTypeJeu() {
        return typeJeu;
    }

    public void setTypeJeu(TypeJeu typeJeu) {
        this.typeJeu = typeJeu;
        this.typeJeu.initModeleJeu(this);
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public GraphiqueStrategy getGraphiqueStrategy() {
        return graphiqueStrategy;
    }

    public void setGraphiqueStrategy(GraphiqueStrategy graphiqueStrategy) {
        this.graphiqueStrategy = graphiqueStrategy;
    }

    public DeplacementStrategy getDeplacementStrategy() {
        return deplacementStrategy;
    }

    public void setDeplacementStrategy(DeplacementStrategy deplacementStrategy) {
        this.deplacementStrategy = deplacementStrategy;
    }

    public GestionJeux getGestionJeux() {
        return gestionJeux;
    }

    public void setGestionJeux(GestionJeux gestionJeux) {
        this.gestionJeux = gestionJeux;
    }

    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    
    
	/**
	 * @return the host
	 */
	public boolean isHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(boolean host) {
		this.host = host;
	}
	
	/**
	 * @return the host
	 */
	public boolean isCoop() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setCoop(boolean host) {
		this.host = host;
	}
	

}
