package model;

import java.io.Serializable;

/**
 * Représente un joueur
 */
public class Joueur implements Serializable{
    private String pseudo;
    private int score;
    private int nbcoups;
    private int meilleurScore=0;

    /**
     * Permet de créer une instance de Joueur
     * @param pseudo
     */
	public Joueur(String pseudo){
        this.pseudo = pseudo;
        score = 0;
    }

    public void ajouterPoints(int points){
        this.score += points;
    }
    
    public void ajouterCoup(){
        this.nbcoups += 1;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    public int getNbcoups() {
		return nbcoups;
	}

	public void setNbcoups(int nbcoups) {
		this.nbcoups = nbcoups;
	}

	/**
	 * @return the meilleurScore
	 */
	public int getMeilleurScore() {
		return meilleurScore;
	}

	/**
	 * @param meilleurScore the meilleurScore to set
	 */
	public void setMeilleurScore(int meilleurScore) {
		this.meilleurScore = meilleurScore;
	}

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

}
