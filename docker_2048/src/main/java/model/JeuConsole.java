package model;

import controller.ControlleurDeplacementConsole;
import view.AffichageConsole;

/**
 * Représente un Jeu de type console
 */
public class JeuConsole extends Jeu {
    private static JeuConsole jeuConsole;

    private JeuConsole(){
        super(AffichageConsole.getInstance(), new ControlleurDeplacementConsole());

    }

    /**
     * Permet de récupérer l'instance de JeuConsole
     * @return
     */
    public static synchronized JeuConsole getJeuConsole(){
        if(jeuConsole == null){
            jeuConsole = new JeuConsole();
        }
        return jeuConsole;
    }

}
