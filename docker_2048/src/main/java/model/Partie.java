package model;

import java.io.Serializable;
import java.util.ArrayList;

import jeu.GestionJeux;
import jeu.Grille;

/**
 * Une partie correspond à l'ensemble des grilles du jeu, c'est ici que sont effectué les déplacement des tuiles dans les grilles
 * Il peut y avoir plusieurs joueur sur une même partie si il s'agit du mode multijoueur coopératif
 */
public class Partie implements Serializable{
    /**
     * ArrayList<Grille>
     * Correspond aux n grilles présentes dans la partie
     */
    private ArrayList<Grille> grilles;
    
    private boolean demandeDeplacement=true;

    private Joueur joueur;

	private boolean isModif;
	private boolean partieGagne,partiePerdu;
	private int directionCourante;
    private int nbTours;
    private String gamemode;
    //attribut qui signifie si le jeu viens d'etre chargé
    private boolean charge;
    /*
    Pour le multijoueur permet de faire du tour par tour
     */
    private boolean autorisationDeJouer;
	
	
    /**
     * Permet la création d'une partie en solo ou multijoueur coopératif
     * @param nbGrille
     * @param joueur
     */
    public Partie(int nbGrille, Joueur joueur){
        this.autorisationDeJouer = true;
        this.partieGagne = false;
        this.partiePerdu = false;
        this.grilles = new ArrayList<Grille>();
        this.joueur = joueur;
        this.nbTours = -2;
        this.gamemode = "SOLO";
        for(int i = 0; i < nbGrille; i++){
            this.grilles.add(new Grille(3));
        }
    }

    /**
     * Permet de créer une partie avec un nombre de grille donnée
     * @param nbGrille
     */
    public Partie(int nbGrille){
        this.autorisationDeJouer = true;
        this.partieGagne = false;
        this.partiePerdu = false;
        this.grilles = new ArrayList<Grille>();
        this.gamemode = "SOLO";
        this.nbTours = -2;

        for(int i = 0; i < nbGrille; i++){
            this.grilles.add(new Grille(3));
        }
        this.joueur = new Joueur("NONAME");
    }

    
	/**
	 * method qui retourne si la partie est prette a recevoir un nouveau déplacement
	 * @return
	 */
    public boolean isDemandeDeplacement() {
        return demandeDeplacement;
    }

    /**
     * methode qui modifie l'attente de la partie au déplacement
     * @param demandeDeplacement
     */
    public void setDemandeDeplacement(boolean demandeDeplacement) {
        this.demandeDeplacement = demandeDeplacement;//demandeDeplacement;
    }


    /**
     * Déplacer les cases d'une grille en fonction d'une direction
     * Deplacement intra-grille seulement
     * @param direction
     *      Integer
     */
    public void deplacerCasesIntraGrille(int direction){
        int score = 0;
        //permet de savoir de quel direction vienne les case
        this.directionCourante=direction;
        
        //ici pour les case intragrille on deplacera des cases plus proche de l'arrivée a celle les plus éloigées
        for(int i = 0; i < this.grilles.size(); i++){
            Grille grille = this.grilles.get(i);
            score += grille.deplacerCases(direction);
        }
        this.joueur.ajouterCoup();
        this.joueur.ajouterPoints(score);
    }
    

    /**
     * Permet le déplacement Intergrille à gauche et à droite
     * @param direction
     *      5 : GAUCHE  6: DROITE
     */
    public void deplacerCasesInterGrille(int direction){
    	boolean trouve = false;
        int score = 0;
        Grille grilleDestination;
        switch(direction){
            case GestionJeux.INTERGRIDDROITE:
                /*
                On souhaite déplacer toutes les tuile vers la grille la plus a droite
                 */
                //Code à séparer
                //On ne fait pas d'opération sur la grille la plus à droite
                for(int k = this.getGrilles().size()-2; k >= 0;k--){
                    //On traite les grille de droite à gauche
                    Grille grille = this.getGrilles().get(k);
                    //Il faut comparer chaque tuile et voir si le déplacement est possible
                    Case[][] tuiles = grille.getCases();

                    //On parcours les tuiles de la grille courante
                    for(int i = 0; i < tuiles.length; i++){

                        for(int j = 0; j < tuiles[i].length; j++){
                            trouve = false;
                            grilleDestination = this.getGrilles().get(this.getGrilles().size()-1);
                            Case tuileGrilleSource = tuiles[i][j];

                            if(tuileGrilleSource instanceof CaseTuile){
                                //La case est une tuile
                                int coordX = tuileGrilleSource.getX();
                                int coordY = tuileGrilleSource.getY();
                               
                                int ite = this.getGrilles().size()-1;

                                //Tant que l'on a pas trouvé d'emplacement disponible et qu'il existe une grille plus à droite que la grille actuelle
                                while(!trouve && ite >= 1 && ite != k){
                                    if(grilleDestination.isCaseAtCoord(coordY,coordX)){
                                        //Une tuile à été trouvé dans la case de destination
                                        if(grilleDestination.isNumberBetweenCaseSame((CaseTuile) tuileGrilleSource, (CaseTuile)grilleDestination.getCases()[coordY][coordX])){
                                            //Les tuiles ont le même nombre on peut donc les fusionner
                                        	
                                            //On supprime la case de la grille source
                                            grille.supprimerUneTuile(coordX, coordY);

                                            //On met à jour le score de la case de destination
                                            try {
                                                score += grilleDestination.mettreAJourNumeroTuile(coordX,coordY);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            trouve = true;                                
                                        }else{
                                            //Les tuiles ont un numéro différent donc le deplacement n'est pas possible, on essaye avec une autre grille
                                            ite--;
                                            grilleDestination = this.getGrilles().get(ite);
                                        }
                                    }else{
                                        //Aucune tuile n'a été trouvé dans la case de destination
                                        //on effectue juste un déplacement
                                        grilleDestination.ajouterTuile((CaseTuile) tuiles[coordY][coordX]);
                                        grille.supprimerUneTuile(coordX,coordY);
                                        trouve = true;
                                    }
                                }
                            }
                        }
                    }
                }
                //

                break;
            case GestionJeux.INTERGRIDGAUCHE :
                //On ne fait pas d'opération sur la grille la plus à gauche
                for(int k = 1; k < this.getGrilles().size();k++){
                    //On traite les grille de gauche à droite
                    Grille grille = this.getGrilles().get(k);
                    //Il faut comparer chaque tuile et voir si le déplacement est possible
                    Case[][] tuiles = grille.getCases();

                    //On parcours les tuiles de la grille courante
                    for(int i = 0; i < tuiles.length; i++){

                        for(int j = 0; j < tuiles[i].length; j++){

                            grilleDestination = this.getGrilles().get(0);
                            Case tuileGrilleSource = tuiles[i][j];

                            if(tuileGrilleSource instanceof CaseTuile){
                                //La case est une tuile
                                int coordX = tuileGrilleSource.getX();
                                int coordY = tuileGrilleSource.getY();
                                trouve = false;
                                int ite = 0;
                                //grilleDestination != grilleSource
                                //Tant que l'on a pas trouvé d'emplacement disponible et qu'il existe une grille plus à droite que la grille actuelle
                                while(!trouve && ite < 2 && ite!=k){
                                    if(grilleDestination.isCaseAtCoord(coordY,coordX)){
                                        //Une tuile à été trouvé dans la case de destination
                                        if(grilleDestination.isNumberBetweenCaseSame((CaseTuile) tuileGrilleSource, (CaseTuile)grilleDestination.getCases()[coordY][coordX])){
                                            //Les tuiles ont le même nombre on peut donc les fusionner

                                            //On supprime la case de la grille source
                                            grille.supprimerUneTuile(coordX, coordY);
                                            //On met à jour le score de la case de destination
                                            try {
                                                score += grilleDestination.mettreAJourNumeroTuile(coordX,coordY);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            trouve = true;
                                        }else{
                                            //Les tuiles ont un numéro différent donc le deplacement n'est pas possible, on essaye avec une autre grille
                                            ite++;
                                            grilleDestination = this.getGrilles().get(ite);
                                        }
                                    }else{
                                        //Aucune tuile n'a été trouvé dans la case de destination
                                        //on effectue juste un déplacement
                                    	//grilleDestination.getCases()[][]
                                        grilleDestination.ajouterTuile((CaseTuile) tuiles[coordY][coordX]);
                                        grille.supprimerUneTuile(coordX,coordY);
                                        trouve = true;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
        }
        
        /*
         * Ici trouve permet de nous avertir si on effectuer un mouvement, si oui, comme on est en intergrille
         * alors on dit a toute les grilles qu'elles ont effectué un deplacement afin de pouvoir créer une tuile 
         * aléatoire a la fin du workflow 
         */
       /* if(trouve) {
        	for(Grille g:this.grilles) {
        		g.setDeplacementEffectue(true);
        	}
        }*/
        //on ajoute les point du joueur
       // this.joueur.ajouterCoup();
        this.joueur.ajouterPoints(score);
    }

    /**
     * Permet de savoir si un déplacement à été effectué dans une grille
     * @return
     */
    public boolean verifierDeplacement(){
        boolean res = false;

        for(int i = 0 ; i < this.getGrilles().size();i++){
            Grille grille = this.getGrilles().get(i);
            if(grille.isDeplacementEffectue()){
                res=true;
            }
            grille.setDeplacementEffectue(false);
        }
        return res;
    }


    public ArrayList<Grille> getGrilles() {
        return grilles;
    }


    /**
     * Permet de vérifier si la partie est terminé
     * @return
     * Vrai alors la aprtie est temriné, faux sinon
     */
    public boolean verifierSiTermine(){
        boolean estTermine = false;


        //On vérifie s'il reste des cases vides et des déplacements possibles
        for(int i = 0; i < this.getGrilles().size(); i++){
            Grille grille = this.getGrilles().get(i);

            estTermine = grille.verifier2048();
            if(estTermine){
                this.partieGagne = true;
            }
            //System.out.println("2048 : "+this.partieGagne);

            if(!this.partieGagne && (!grille.verifierSiCaseVide() && !grille.verifierSiFusionPossible())){
                estTermine = true;
            }

            if(partieGagne || !estTermine){
                break;
            }
        }
        //System.out.println("Boucle grille etat : "+estTermine);
        if(estTermine && !this.partieGagne){
            estTermine = !this.verifierSiDeplacementInterGrillePossible();
        }
        //System.out.println("intergrille: "+estTermine);

        if(estTermine && !this.partieGagne){
            this.partiePerdu = true;
        }

        return estTermine;
    }


    /**
     * Permet de vérifier si des déplacement intergrille sont encore possible (au moins un)
     * @return
     * Vrai, au moins un déplacement est encore possible. Faux sinon.
     */
    public boolean verifierSiDeplacementInterGrillePossible(){
        boolean res = false;

        for(int i = 0; i < this.getGrilles().size(); i++){
            Grille grilleA = this.getGrilles().get(i);

            for(int j = 0; j < this.getGrilles().size(); j++){
                if(j != i){
                    Grille grilleB = this.getGrilles().get(j);
                    //On parcours toutes les cases de grilleA

                    for(int k = 0; k < grilleA.getCases().length; k++){
                        for(int l = 0; l < grilleA.getCases()[k].length;l++){

                            Case caseA = grilleA.getCases()[k][l];
                            Case caseB = grilleB.getCases()[k][l];

                            if(caseA instanceof CaseTuile){

                                //Fusion encore possible
                                if(caseB instanceof CaseTuile && (((CaseTuile)caseA).getNombre() == ((CaseTuile)caseB).getNombre())){
                                    res = true;
                                }
                            }
                            if(res){
                                break;
                            }
                        }
                        if(res){
                            break;
                        }
                    }
                    if(res){
                        break;
                    }
                }
            }
            if(res){
                break;
            }
        }

        return res;
    }


    public void setGrilles(ArrayList<Grille> grilles) {
        this.grilles = grilles;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    public String toString() {
        String result = "";

        //Affichage du score
        result += "Score : "+ this.joueur.getScore()+"\n\n";


        //Affichage des grilles
        for(int i = 0; i < this.getGrilles().get(0).getCases().length;i++){
            for(int j = 0; j < this.getGrilles().size();j++){
                Grille grille = this.getGrilles().get(j);
                result += grille.getRepesentationOfLine(i);
                result += " | ";
            }
            result += "\n";
        }

        return result;
    }

    /**
     * Permet d'intervertir l'autorisation de jouer
     */
    public void switchAutorisationJouer(){
        if(this.autorisationDeJouer){
            this.autorisationDeJouer = false;
        }else{
            this.autorisationDeJouer = true;
        }
    }

	public boolean isModif() {
		return this.isModif;
	}
	
	/**
	 * method qui permet de mettre a jour si la partie a ete mis a jour ou non selon l'iteration courante 
	 * @param m
	 */
	public void setModif(boolean m) {
		this.isModif=m;
		this.charge=false;
	}

	public int getDirectionCourante() {
		return this.directionCourante;
	}

    public int getNbTours() {
        return nbTours;
    }

    public void setNbTours(int nbTours) {
        this.nbTours = nbTours;
    }

    public boolean isAutorisationDeJouer() {
        return autorisationDeJouer;
    }

    public void setAutorisationDeJouer(boolean autorisationDeJouer) {
        this.autorisationDeJouer = autorisationDeJouer;
    }

    /**
     * booléen qui dit si la partie viens d'etre chargé
     * @param b
     */
	public void setCharge(boolean b) {
		this.isModif=false;
		this.charge=b;
		
	}
	
	/**
	 * getteur si la partie viens d'etre chargé ou non
	 * @return
	 */
	public boolean isCharge() {
		return this.charge;
	}

    public String getGamemode() {
        return gamemode;
    }

    public void setGamemode(String gamemode) {
        this.gamemode = gamemode;
    }

    public boolean isPartieGagne() {
        return partieGagne;
    }

    public void setPartieGagne(boolean partieGagne) {
        this.partieGagne = partieGagne;
    }

    public boolean isPartiePerdu() {
        return partiePerdu;
    }

    public void setPartiePerdu(boolean partiePerdu) {
        this.partiePerdu = partiePerdu;
    }
}
