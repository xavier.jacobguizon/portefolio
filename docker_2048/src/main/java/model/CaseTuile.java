package model;

/**
 * Représente une case de type Tuile
 */
public class CaseTuile extends Case{
    /**
     * Integer
     * Correspond au nombre présent sur la case
     */
    private int nombre;
    private double xAffichagePrecedent=-1;
    private double yAffichagePrecedent=-1;
    private boolean nouveau=true;

    /**
     * Permet d'instancier une case Tuile
     * @param x
     * @param y
     */
    public CaseTuile(int x, int y){
        super(x,y);
        this.nombre = 2;
    }

    /**
     * Permet d'instancier une case tuile en précisant son nombre
     * @param x
     * @param y
     * @param nombre
     */
    public CaseTuile(int x, int y, int nombre){
        super(x,y);
        this.nombre = nombre;
    }

    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    public void setPositionAffichage(double x,double y) {
    	this.xAffichagePrecedent=x;
    	this.yAffichagePrecedent=y;
    }
    
    public double getXAffichage() {
    	return this.xAffichagePrecedent;
    }
    
    public double getYAffichage() {
    	return this.yAffichagePrecedent;
    }
    
    /**
     * method qui permet de modifier la case pour qu'elle ne soit plus signifié comme  gvbnouvelle
     */
    public void utilise() {
    	this.nouveau=false;
    }
    
    public boolean getNouveau() {
    	return this.nouveau;
    }
}
