package com.mycompany.app;

import javafx.scene.image.Image;

import controller.ControlleurMenu;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import jeu.PreparationJeu;

/**
 * Classe principale
 */
public class App extends Application{
	
    public static void main(String[] args) {
        PreparationJeu preparationJeu = PreparationJeu.getPreparationJeu();
        preparationJeu.lancerLauncher();
    }

    @Override /*inutile, juste pour satisfaire extends Application ici*/
    public void start(Stage primaryStage) throws Exception {
        //recuperation de la ressource fxml et chargement de cette derniere
        FXMLLoader loader = new FXMLLoader(view.AffichageInterface.class.getResource("/MenuPseudo.fxml"));
        Parent root = loader.load();

        ControlleurMenu cm = loader.getController();
        //System.out.println(cm);
        cm.setJeu(PreparationJeu.getPreparationJeu().getJeu());
        
        //tentative d'implémentation d'icone pour la fenetre
        //ImageIcon image = new ImageIcon(getClass().getResource("/com/rdn.png"));
        //this.setIconImage(image.getImage());
        //primaryStage.getIcons().add(new Image("../img/theme2048.png"));
        Image i = new Image(getClass().getResourceAsStream("/img/2048.png"));
    
        primaryStage.getIcons().add(i);
        primaryStage.setTitle("2048 3D");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
