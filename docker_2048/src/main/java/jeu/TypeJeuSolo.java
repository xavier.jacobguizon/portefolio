package jeu;

import controller.DeplacementStrategy;
import view.GraphiqueStrategy;

/**
 * Représente le type de jeu solo
 */
public class TypeJeuSolo extends TypeJeu {

    private boolean premierAffichage;
    /**
     * Créer le moteur de jeu pour une partie solo
     *
     * @param deplacementStrategy
     * @param graphiqueStrategy
     * @param gestionJeux
     */
    public TypeJeuSolo(DeplacementStrategy deplacementStrategy, GraphiqueStrategy graphiqueStrategy, GestionJeux gestionJeux) {
        super(deplacementStrategy, graphiqueStrategy, gestionJeux);
        this.premierAffichage = true;
    }


    /**
     * Permet la mise à jour des modéles pour ce type de jeu
     */
    @Override
    public void update() {
            if (this.getDeplacementStrategy() != null && !this.premierAffichage) {
                this.getDeplacementStrategy().choisirProchainDeplacement();
            }
            //On vérifie si la partie est terminé
            if(this.getGestionJeux().getPartie().verifierSiTermine()){
                System.out.println("Partie terminé");
                //Dernier affichage du jeu
                this.getGraphiqueStrategy().update();
                if(this.getGestionJeux().getPartie().isPartieGagne()){
                    //la partie est gagné
                    this.getGraphiqueStrategy().finDePartie(true);
                }else if(this.getGestionJeux().getPartie().isPartiePerdu()){
                    //La partie est perdu
                    this.getGraphiqueStrategy().finDePartie(false);
                }

                this.getJeu().terminerLeJeu();
            }

    }

    /**
     * Permet la mise à jour de l'affichage pour ce type de jeu
     */
    @Override
    public void display() {
        if (this.getGestionJeux().getPartie().isModif() || this.premierAffichage) {
            this.premierAffichage = false;
            try {
                if (this.getGraphiqueStrategy() != null && this.getGestionJeux() != null)
                    this.getGraphiqueStrategy().update();
            } catch (Exception e) {
                System.out.println(e);
                this.getTimer().stop();
            }
        }

    }

    
    /**
     * method initialise l'option graphique, le controleur ainsi que le model du controleur et lance une partie en précisant qu'elle est modifié
     */
    @Override
    public void preparerEnvironnementDeJeu(DeplacementStrategy ds) {
        this.getGraphiqueStrategy().initGraphics(this.getGestionJeux().getPartie());
        this.setDeplacementStrategy(ds);
        this.getDeplacementStrategy().initModele(this.getGestionJeux());
        this.getGestionJeux().setJeuEnCours(true);
        this.getGestionJeux().getPartie().setModif(true);
    }

    /**
     * Permet de préparer l'environnement de jeu pour le mode console
     */
    @Override
    public void preparerEnvironnementDeJeu() {
        this.getGraphiqueStrategy().initGraphics(this.getGestionJeux().getPartie());
        this.getDeplacementStrategy().initModele(this.getGestionJeux());
    }
}
