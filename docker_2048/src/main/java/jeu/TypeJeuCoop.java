package jeu;

import controller.DeplacementStrategy;
import model.Partie;
import multijoueur.Client;
import multijoueur.Message;
import multijoueur.Serveur;
import multijoueur.ServeurCoop;
import view.GraphiqueStrategy;

/**
 * Représente un mode de jeu coopératif
 */
public class TypeJeuCoop extends TypeJeu {
    /**
     * Instance du serveyr
     */
    private Serveur serveur;
    /**
     * Instance du client
     */
    private Client client;
    private boolean synchroVue;
    private boolean synchroControlleur;
    /**
     * Permet de savoir si la partie est terminé
     */
    private boolean partieTermine;


    /**
     * Créer le moteur de jeu pour une partie en coopération
     *
     * @param deplacementStrategy
     * @param graphiqueStrategy
     * @param gestionJeux
     */
    public TypeJeuCoop(DeplacementStrategy deplacementStrategy, GraphiqueStrategy graphiqueStrategy, GestionJeux gestionJeux) {
        super(deplacementStrategy, graphiqueStrategy, gestionJeux);
        this.synchroVue = false;
        this.synchroControlleur = false;
        this.partieTermine = false;
        this.getGestionJeux().getPartie().setGamemode("COOP");
    }


    /**
     * Permet la mise à jour des modéles pour ce type de jeu
     */
    @Override
    public void update() {
       //Si il on a recu un nouveau message et qu'il s'agit d'une partie
        if(this.getClient().isNouveauMessage() && this.getClient().getSujetDernierMessage() == Message.ENVOI_PARTIE){
            //On a reçu une partie
            this.getGestionJeux().setPartie(this.getClient().getPartieRecu());
            this.getGraphiqueStrategy().initGraphics(this.getGestionJeux().getPartie());
            //On demande à la vue d'afficher
            this.synchroVue = true;
            this.getClient().setNouveauMessage(false);
        }

        //On vérifie si la partie est terminé
        if(this.getClient().isNouveauMessage()){
            if(this.getClient().getSujetDernierMessage() == Message.END_LOSE){
                //La partie est perdu
                this.partieTermine = true;
                this.getGraphiqueStrategy().finDePartie(false);
                this.getJeu().terminerLeJeu();
            }else if(this.getClient().getSujetDernierMessage() == Message.END_WIN){
                //la partie est gagné
                this.partieTermine = true;
                this.getGraphiqueStrategy().finDePartie(true);
                this.getJeu().terminerLeJeu();
            }
        }

        //Si on est autorisé à jouer alors on permet le déplacement
        Partie maPartie = this.getGestionJeux().getPartie();
        if (maPartie.isAutorisationDeJouer() && synchroControlleur && !partieTermine) {
            //Remplacer par une action de la vue !!!!
            System.out.println("C'est à vous de jouer ! ");
            //On autorise le déplacement
            if (this.getDeplacementStrategy() != null) {
                this.getDeplacementStrategy().choisirProchainDeplacement();
            }

            //Si il y a eu un déplacement alors on envoie la partie au serveur
            if (this.getGestionJeux().getPartie().isModif()) {
                synchroControlleur = false;
                this.getGestionJeux().getPartie().setModif(false);
                //On a effectuer un déplacement, on n'est plus autorisé à ce déplacer
                this.getGestionJeux().getPartie().setAutorisationDeJouer(false);

                //Envoie de la partie
                this.client.envoyerPartie(this.getGestionJeux().getPartie());

                //On vérifie si la partie est termine
                if(this.getGestionJeux().getPartie().verifierSiTermine()){
                    if(this.getGestionJeux().getPartie().isPartiePerdu()){
                        //La partie est perdu
                        this.getClient().envoyerFinDePartie(false);
                        this.getGraphiqueStrategy().finDePartie(false);
                    }else if(this.getGestionJeux().getPartie().isPartieGagne()){
                        this.getClient().envoyerFinDePartie(true);
                        this.getGraphiqueStrategy().finDePartie(true);
                    }
                    this.getJeu().terminerLeJeu();
                }
            }

        }else{
            synchroControlleur = false;
        }
    }

    /**
     * Permet la mise à jour de l'affichage pour ce type de jeu
     */
    @Override
    public void display() {
        /*
        Voir ici pour ajouter un effet pour signifier le changement de tour
         */
        //Si on a reçu une partie ou il y a eu modif


        if(synchroVue || this.isPremierAffichage()){
            //On affiche le jeu et on demande la prochain mouvement
            this.setPremierAffichage(false);
            try {
                this.getGraphiqueStrategy().update();
                this.synchroVue = false;
                this.synchroControlleur = true;
            } catch (Exception e) {
                this.getTimer().stop();
            }
        }
    }

    @Override
    public void preparerEnvironnementDeJeu(DeplacementStrategy ds) {

    }

    @Override
    public void preparerEnvironnementDeJeu() {
        this.getGraphiqueStrategy().initGraphics(this.getGestionJeux().getPartie());
        this.getDeplacementStrategy().initModele(this.getGestionJeux());
    }

    /**
     * Permet la création d'un serveur lors de l'hebergement d'une partie Coop
     *
     * @param nomServeur
     */
    public void hebergerUnePartie(String nomServeur) {
        this.serveur = new ServeurCoop(nomServeur);
        System.out.println("Lancement du serveur en cours . . .");
        //this.serveur.lancerServeur();
        Thread serveurThread = new Thread(serveur);
        serveurThread.start();
        this.client = new Client("0.0.0.0", this.serveur.getPort());
        Thread clientTh = new Thread(this.client);
        clientTh.start();
        this.client.envoyerPartie(this.getGestionJeux().getPartie());
        this.getClient().setReady();
    }

    /**
     * Permet de rejoindre une partie
     *
     * @param host
     * @param port
     */
    public void rejoindreUnePartie(String host, int port) {
        this.client = new Client(host, port);
        Thread clientTh = new Thread(this.client);
        clientTh.start();
        //On doit récupérer la partie distante
        System.out.println("Connexion en cours . . .");
        System.out.println("Récupération de la partie distante . . .");
        boolean partieRecupere = false;

        while (!partieRecupere) {
            this.getClient().demanderPartieAutreJoueur();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (this.getClient().isNouveauMessage()) {
                this.getClient().setNouveauMessage(false);
                if (this.getClient().getSujetDernierMessage() == Message.ENVOI_PARTIE) {
                    partieRecupere = true;
                    this.getClient().setReady();
                    this.getGestionJeux().setPartie(this.getClient().getPartieRecu());
                    this.getGraphiqueStrategy().initGraphics(this.getGestionJeux().getPartie());
                    this.getDeplacementStrategy().initModele(this.getGestionJeux());
                    this.getGestionJeux().getPartie().setAutorisationDeJouer(false);
                }
            }
        }
    }

    public Serveur getServeur() {
        return serveur;
    }

    public void setServeur(Serveur serveur) {
        this.serveur = serveur;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
