package jeu;

import controller.DeplacementStrategy;
import model.Jeu;
import view.GraphiqueStrategy;
import javafx.animation.AnimationTimer;

/**
 * Design Pattern Strategy
 * Concerne le Type du jeu
 */
public abstract class TypeJeu {

    /**
     * Type du déplacement
     */
    private DeplacementStrategy deplacementStrategy;
    /**
     * Type du mode graphique
     */
    private GraphiqueStrategy graphiqueStrategy;
    /**
     * Permet la gestion du jeu
     */
    private GestionJeux gestionJeux;
    private AnimationTimer timer;
    /**
     * Instance du jeu
     */
    private Jeu jeu;
    /**
     * Permet de savoir si le jeu est affiché pour la premiere fois
     */
    private boolean premierAffichage;


    /**
     * temps courant au début d'un cycle
     * commence au debut du jeu
     */
    private long tDebut=System.currentTimeMillis();

    /**
     * permet de mesurer si une seconde est passé
     */
    private boolean sec=false;

    /**
     * permet de mesurer le temps d'un cycle, si elle est égale au supérieur a tempAction, on reset et incremente loop
     */
    private long tCycle=System.currentTimeMillis();

    /**
     * on definit ici les fps qu'on souhaite pour notre jeu
     */
    private long framesPerSecond=(long) 60.00;

    /**
     * on definit ici a partir du nombre de fps le cycle d'action de notre jeu
     * a chaque fois que ce temps est passé on fait une boucle dans le moteur
     */
    private long tempAction=1000/framesPerSecond;

    /**
     * nombre d'action effectuer par cycle, il sera incrementer a chaque cycle
     * permet de mesurer le nombre de fps effectif
     */
    //private int loop=0;


    /**
     * Permet la création d'un type de jeu
     * @param deplacementStrategy
     * @param graphiqueStrategy
     * @param gestionJeux
     */
    public TypeJeu(DeplacementStrategy deplacementStrategy, GraphiqueStrategy graphiqueStrategy, GestionJeux gestionJeux){
        this.deplacementStrategy = deplacementStrategy;
        this.graphiqueStrategy = graphiqueStrategy;
        this.gestionJeux = gestionJeux;
        this.premierAffichage = true;
    }

    /**
     * Permet d'initialisé le modéle Jeu
     * @param jeu
     */
    public void initModeleJeu(Jeu jeu){
        this.jeu = jeu;
    }


    /**
     * Permet la mise à jour du modéle
     */
    public abstract void update();

    /**
     * Permet la mise à jour de l'affichage
     */
    public abstract void display();


    /**
     * Permet de mettre l'interface du jeu (Affichage des grilles etc....)
     * Seulement dans un mode graphique
     */
    public abstract void preparerEnvironnementDeJeu(DeplacementStrategy ds);

    /**
     * Permet de mettre l'interface de jeu
     * Seulement dans un mode console
     */
    public abstract void preparerEnvironnementDeJeu();


    /**
     * Permet le lancement du moteur de jeu
     */
    public void startTimer(){
        timer=new AnimationTimer() {

            @Override
            public void handle(long arg0) {
                //on verifie si la seconde est passé
                if((System.currentTimeMillis()-tDebut)/1000 >=1)	{
                    tDebut=System.currentTimeMillis();sec=true;
                }
                //on verifie si un cycle est passé affiche le jeu
                if((System.currentTimeMillis()-tCycle)>=tempAction) {
                    display();
                    tCycle=System.currentTimeMillis();
                }
                //try {
                //loop++;
                update();
				/*}catch(ExceptionFinJeu e) {

				}*/
                //verifie qu'on a passé la seconde
                if(sec) {
                    //fps.setText("fps : "+loop);
                    sec=false;//loop=0;
                }
            }
        };
        timer.start();
    }

    /**
     * Permet de stopper le moteur de jeu
     */
    public void stopTimer(){
        this.timer.stop();
    }

    public DeplacementStrategy getDeplacementStrategy() {
        return deplacementStrategy;
    }

    public void setDeplacementStrategy(DeplacementStrategy deplacementStrategy) {
        this.deplacementStrategy = deplacementStrategy;
    }

    public GraphiqueStrategy getGraphiqueStrategy() {
        return graphiqueStrategy;
    }

    public void setGraphiqueStrategy(GraphiqueStrategy graphiqueStrategy) {
        this.graphiqueStrategy = graphiqueStrategy;
    }

    public GestionJeux getGestionJeux() {
        return gestionJeux;
    }

    public void setGestionJeux(GestionJeux gestionJeux) {
        this.gestionJeux = gestionJeux;
    }

    public AnimationTimer getTimer() {
        return timer;
    }

    public void setTimer(AnimationTimer timer) {
        this.timer = timer;
    }

    public boolean isPremierAffichage() {
        return premierAffichage;
    }

    public void setPremierAffichage(boolean premierAffichage) {
        this.premierAffichage = premierAffichage;
    }

    public Jeu getJeu() {
        return jeu;
    }

    public void setJeu(Jeu jeu) {
        this.jeu = jeu;
    }
}
