package jeu;

import java.util.Scanner;

import model.Jeu;
import model.JeuConsole;
import model.JeuGraphique;

/**
 * Classe gérant le lancement du jeu et le choix de l'interface graphique
 * Classe Singleton
 */
public class PreparationJeu {

    /**
     * Instance de PreparationJeu
     */
    private static PreparationJeu preparationJeu;
    /**
     * Jeu en cours
     */
    private Jeu jeu;

    private PreparationJeu(){}

    /**
     * Permet de récupérer l'instance de PreparationJeu
     * @return Preparationjeu
     */
    public static synchronized PreparationJeu getPreparationJeu(){
        if(preparationJeu == null){
            preparationJeu = new PreparationJeu();
        }
        return preparationJeu;
    }

    /**
     * Permet le lancement du jeu
     * Permet de choisir l'interface console ou graphique
     */
    public void lancerLauncher(){
        /*
        Demande du type graphique pour déterminé le jeu : JeuGrpahique ou JeuConsole
         */
        /* Choix du mode graphique*/
        System.out.println("Bienvenue, veuillez séléctionner un mode graphique : \n1.Mode console\n2.Mode Graphique");

        Scanner sc = new Scanner(System.in);
            /*choix du joueur avec vérification d'une entrée acceptable (entier et dans l'intervale)*/
            int choix = -1;

            while (choix < 1 || choix > 2) {
                System.out.println("Veuillez rentrer une réponse entre 1 et 2");
                while (!sc.hasNextInt()) {
                    System.out.println("Veuillez rentrer une réponse correct");
                    sc.next();
                }

                choix = sc.nextInt();
            }
            /*d'après le choix du joueur creation du mode de jeu, en console (1) ou non(2)*/
            switch (choix) {

                /*Choix du mode console, on récupere l'instance de "AffichageConsole" et on créer un controlleur adapté*/
                case 1:
                    System.out.println("Vous avez choisi le mode console !");
                    this.jeu = JeuConsole.getJeuConsole();
                    break;

                case 2:
                    System.out.println("Vous avez choisi le mode graphique\nLancement en cours...");
                    this.jeu = JeuGraphique.getJeuGraphique();
                    break;
            }
        
        //On affiche le menu principal et on intilialise
        this.jeu.getMenu().setJeu(this.jeu);
        this.jeu.changerDeMenu(Menu.MENU_PSEUDO);
    }


    public Jeu getJeu() {
        return jeu;
    }

    public void setJeu(Jeu jeu) {
        this.jeu = jeu;
    }
}
