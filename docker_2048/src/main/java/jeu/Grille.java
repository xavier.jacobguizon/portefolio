package jeu;

import java.io.Serializable;
import model.Case;
import model.CaseTuile;
import model.CaseVide;

/**
 * Représente une grille dans la jeu 2048
 */
public class Grille implements Serializable{


    /**
     * Case[][]
     * Correspond aux cases présente dans une grille
     */
    private Case[][] cases;


    /**
     * Indique si un déplacement à eu lieu dans cette grille
     */
    private boolean deplacementEffectue;

    /**
     * Permet de créer une instance de Grille
     * @param tailleCarre
     *          Permet de spécifier la taille d'une grille
     */
    public Grille(int tailleCarre) {
        this.deplacementEffectue = false;
        this.cases = new Case[tailleCarre][tailleCarre];
        for (int i = 0; i < cases.length; i++) {
            for (int j = 0; j < this.cases[i].length; j++) {
                this.cases[i][j] = new CaseVide(j, i);
            }
        }
    }


    /**
     * Permet le déplacement de case au seins de cette grille
     * @param direction
     *      Correspond aux identifiants de la classe GestionJeux.class
     * @return
     */
    public int deplacerCases(int direction) {
        int pointsObtenu = 0;


        switch (direction) {
            case GestionJeux.HAUT:

                for (int i = 0; i < this.getCases().length; i++) {
                    for (int j = 0; j < this.getCases()[i].length; j++) {

                        Case caseActuelle = this.getCases()[i][j];
                        if (caseActuelle instanceof CaseTuile) {
                            int coordXTest = j;
                            int coordYTest = i;

                            //On cherche un emplacement dans la grille dans une case Vide
                            while (isInsideGrid(coordXTest, coordYTest - 1) && !isCaseAtCoord(coordYTest - 1, coordXTest)) {
                                coordYTest -= 1;
                            }
                            //On vérifie si la case au dessus est dans la grille
                            if (!(caseActuelle.getY() - 1 < 0)) {
                                pointsObtenu += this.procederAuDeplacement(caseActuelle, coordYTest - 1, coordXTest, coordYTest, coordXTest);
                            }
                        }
                    }
                }
                break;


            case GestionJeux.BAS:

                for (int i = this.getCases().length-1; i >= 0; i--) {
                    for (int j = this.getCases()[i].length-1; j >= 0; j--) {

                        Case caseActuelle = this.getCases()[i][j];
                        if (caseActuelle instanceof CaseTuile) {
                            int coordXTest = j;
                            int coordYTest = i;
                            while (isInsideGrid(coordXTest, coordYTest + 1) && !isCaseAtCoord(coordYTest + 1, coordXTest)) {
                                coordYTest += 1;
                            }
                            if (!(caseActuelle.getY() + 1 >= this.getCases().length)) {
                                pointsObtenu += this.procederAuDeplacement(caseActuelle, coordYTest + 1, coordXTest, coordYTest, coordXTest);
                            }
                        }
                    }
                }
                break;


            case GestionJeux.GAUCHE:
                for (int j = 0; j < this.getCases().length; j++) {
                    for (int i = 0; i < this.getCases()[j].length; i++) {

                        Case caseActuelle = this.getCases()[i][j];
                        if (caseActuelle instanceof CaseTuile) {
                            int coordXTest = j;
                            int coordYTest = i;

                            while (isInsideGrid(coordXTest - 1, coordYTest) && !isCaseAtCoord(coordYTest, coordXTest - 1)) {
                                coordXTest -= 1;
                            }
                            if (!(caseActuelle.getX() - 1 < 0)) {
                                pointsObtenu += this.procederAuDeplacement(caseActuelle, coordYTest, coordXTest - 1, coordYTest, coordXTest);
                            }
                        }
                    }
                }
                break;
            case GestionJeux.DROITE:
                for (int j = this.getCases().length-1; j >= 0; j--) {
                    for (int i = this.getCases()[j].length-1; i >= 0 ; i--) {

                        Case caseActuelle = this.getCases()[i][j];
                        if (caseActuelle instanceof CaseTuile) {
                            int coordXTest = j;
                            int coordYTest = i;
                            while (isInsideGrid(coordXTest + 1, coordYTest) && !isCaseAtCoord(coordYTest, coordXTest + 1)) {
                                coordXTest += 1;
                            }
                            if (!(caseActuelle.getX() + 1 >= this.getCases().length)) {
                                pointsObtenu += this.procederAuDeplacement(caseActuelle, coordYTest, coordXTest + 1, coordYTest, coordXTest);
                            }
                        }
                    }
                }

                break;
            default:
                throw new IllegalStateException("Ce déplacement n'est pas un déplacement possible");


        }
        return pointsObtenu;
    }


    /**
     * Permet de proceder au deplacement en changeant la valeur des cases et leurs positions dans la grille
     *
     * @param caseActuelle
     * @param coordY
     * @param coordX
     * @return
     */
    public int procederAuDeplacement(Case caseActuelle, int coordYSuivant, int coordXSuivant, int coordY, int coordX) {
        int pointObtenu = 0;

        if (isCaseAtCoord(coordYSuivant, coordXSuivant)) {
            Case caseTeste = this.getCases()[coordYSuivant][coordXSuivant];
            if (isNumberBetweenCaseSame((CaseTuile) caseActuelle, (CaseTuile) caseTeste)) {    
            		pointObtenu = fusionnerCases((CaseTuile) caseTeste , (CaseTuile) caseActuelle);
                
            }else{
                //Les scores des cases sont différents, la fusion ne peut pas avoir lieux donc on déplace la case à l'emplacement libre trouvé
                deplacerCaseTuileVersCaseVide(caseActuelle,coordY,coordX);
            }
        } else {
            deplacerCaseTuileVersCaseVide(caseActuelle, coordY, coordX);
        }
        
        return pointObtenu;
    }


    /**
     * Permet la fusion de deux cases
     * @param caseA Case qui se déplace (Son score ne sera pas augmenté, cette case sera supprimé)
     * @param caseB Case présente à l'endroit où caseA veut se déplacer (Son score sera augmenté)
     * @return int Le score généré
     */
    public int fusionnerCases(CaseTuile caseB, CaseTuile caseA) {
        int pointObtenu = 0;
        /*
        On change le type de la caseA dans la grille
         */
        this.supprimerUneTuile(caseA.getX(), caseA.getY());
        /*
        On augmente le score de la case B
         */
        try {
            pointObtenu = mettreAJourNumeroTuile(caseB.getX(), caseB.getY());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pointObtenu;
    }

    /**
     * Permet de mettre à jour le numéro d'une tuile
     * @param x
     * @param y
     * @return int le score généré
     * @throws Exception
     */
    public int mettreAJourNumeroTuile(int x, int y) throws Exception {
        int pointObtenus = 0;


        Case tuile = this.getCases()[y][x];
        if (tuile instanceof CaseTuile) {
            this.deplacementEffectue =true;
            ((CaseTuile) this.getCases()[y][x]).setNombre(((CaseTuile) tuile).getNombre() * 2);

            ((CaseTuile) this.getCases()[y][x]).utilise();
            pointObtenus = ((CaseTuile) tuile).getNombre();
        } else {
            throw new Exception("Impossible de mettre à jour une case vide !");
        }
        return pointObtenus;
    }

    /**
     * Permet de vérifier s'il existe au moins une case vide
     * @return
     * Vrai s'il existe au moins une case vide, Faux sinon.
     */
    public boolean verifierSiCaseVide(){
        boolean res = false;

        for(int i = 0; i < this.getCases().length; i++){
            for(int j = 0; j < this.getCases()[i].length; j++){
                Case caseGrille = this.getCases()[i][j];
                if(caseGrille instanceof CaseVide){
                    res = true;
                    break;
                }
            }
            if(res){
                break;
            }
        }

        return res;
    }


    /**
     * Permet de vérifier si on peut encore faire au moins une fusion dans la grille
     * @return
     */
    public boolean verifierSiFusionPossible(){
        boolean res = false;

        for(int i = 0; i < this.getCases().length; i++){
            for(int j = 0; j < this.getCases()[i].length; j++){
                Case caseGrille = getCases()[i][j];
                if(caseGrille instanceof CaseTuile && !res){
                    if((isInsideGrid(j+1,i) && isCaseAtCoord(i,j+1) && isNumberBetweenCaseSame((CaseTuile)caseGrille,(CaseTuile)getCases()[i][j+1])) ||
                            (isInsideGrid(j-1,i) && isCaseAtCoord(i,j-1) && isNumberBetweenCaseSame((CaseTuile)caseGrille,(CaseTuile)getCases()[i][j-1])) ||
                            (isInsideGrid(j,i+1) && isCaseAtCoord(i+1,j) && isNumberBetweenCaseSame((CaseTuile)caseGrille,(CaseTuile)getCases()[i+1][j])) ||
                            (isInsideGrid(j,i-1) && isCaseAtCoord(i-1,j) && isNumberBetweenCaseSame((CaseTuile)caseGrille,(CaseTuile)getCases()[i-1][j]))){
                        res = true;
                    }
                }
            }
        }

        return res;
    }

    /**
     * Permet de vérifier si le joueur à réussi a former un 2048
     * @return
     */
    public boolean verifier2048(){
        boolean res = false;

        for(int i = 0; i < getCases().length; i++){
            for(int j = 0; j < getCases()[i].length; j++){
                Case caseGrille = getCases()[i][j];
                if(caseGrille instanceof CaseTuile && ((CaseTuile) caseGrille).getNombre() == 2048){
                    res = true;
                }

                if(res){
                    break;
                }
            }
            if(res){
                break;
            }
        }

        return res;
    }


    /**
     * Permet le déplacement d'une case Tuile vers une case Vide
     *
     * @param caseActuelle Case en cours déplacement
     * @param newCoordY    Coordonnée Y d'arrivée
     * @param newCoordX    Coordonnée X d'arrivée
     */
    public void deplacerCaseTuileVersCaseVide(Case caseActuelle, int newCoordY, int newCoordX) {
        //On vérifie si il y a une case à la destination
        if(!isCaseAtCoord(newCoordY,newCoordX)){
        	
            this.getCases()[newCoordY][newCoordX] = caseActuelle;
            
            double xPrec=caseActuelle.getX();
            double yPrec=caseActuelle.getY();
            
            this.supprimerUneTuile(caseActuelle.getX(), caseActuelle.getY());
            
            caseActuelle.setCoord(newCoordX, newCoordY);
            deplacementEffectue = true;
            ((CaseTuile)caseActuelle).setPositionAffichage((int)yPrec, (int)xPrec);
        }
    }

    /**
     * Ajoute une case à la grille aux coordonnées de celle-ci
     *
     * @param caseAjoute
     */
    public void ajouterTuile(CaseTuile caseAjoute) {
        this.getCases()[caseAjoute.getY()][caseAjoute.getX()] = caseAjoute;
        //this.deplacementEffectue = true;
    }

    /**
     * Permet de détecter si les coordonnées indiquées sont dans la grille
     * Cette méthode suppose que la grille est un carré.
     *
     * @param x Coordonnées X (j)
     * @param y Coordonnées Y (i)
     * @return Vrai si les coordonnées sont en dans la grille. Faux sinon.
     */
    public boolean isInsideGrid(int x, int y) {
        boolean res = true;

        if (x >= this.getCases()[0].length || y >= this.getCases().length
                || x < 0 || y < 0) {
            res = false;
        }

        return res;
    }

    /**
     * Permet de savoir si une case est présente à des coordonnées
     *
     * @param i
     * @param j
     * @return Vrai si une case tuile est présente. Faux sinon.
     */
    public boolean isCaseAtCoord(int i, int j) {
        boolean caseTrouve = false;

        //On vérifie que les coordonnées sont bien possible dans la grille
        if (isInsideGrid(j, i)) {
            Case caseOnCoord = this.getCases()[i][j];

            if (caseOnCoord instanceof CaseTuile) {
                caseTrouve = true;
            }
        }

        return caseTrouve;
    }

    /**
     * Permet de supprimer une tuile de la grille
     *
     * @param x
     * @param y
     */
    public void supprimerUneTuile(int x, int y) {
        this.getCases()[y][x] = new CaseVide(x, y);
        this.deplacementEffectue = true;
    }

    /**
     * Permet de remplir la grille pour ne permettre aucun déplacement
     */
    public void remplirLaGrille(){
        int random =0;
        for(int i = 0; i<this.getCases().length;i++){
            for(int j = 0; j < this.getCases()[i].length; j++){
                random = (int)(Math.random() * 9999);
                this.getCases()[i][j] = new CaseTuile(j,i,random);
            }

        }
    }

    /**
     * Détermine si les score entre deux cases Tuile sont les mêmes.
     *
     * @param caseA
     * @param caseB
     * @return Vrai si les cases ont le même nombre. Faux sinon.
     */
    public boolean isNumberBetweenCaseSame(CaseTuile caseA, CaseTuile caseB) {
        boolean res = false;
        try{
            if (caseA.getNombre() == caseB.getNombre()) {
                res = true;
            }
        }catch(Exception e){
            res = false;
        }

        return res;
    }


    public Case[][] getCases() {
        return cases;
    }

    public void setCases(Case[][] cases) {
        this.cases = cases;
    }

    public String toString() {
        String res = "";

        for (int i = 0; i < this.getCases().length; i++) {
            for (int j = 0; j < this.getCases()[i].length; j++) {
                Case caseActuelle = this.getCases()[i][j];
                String representationCaseActuelle = "";

                if (caseActuelle instanceof CaseVide) {
                    representationCaseActuelle = "*";
                } else {
                    representationCaseActuelle = ((CaseTuile) caseActuelle).getNombre() + "";
                }
                res += representationCaseActuelle;
            }
            res += "\n";
        }


        return res;
    }

    public String getRepesentationOfLine(int lineNumber) {
        String result = "";

        for (int i = 0; i < this.getCases()[lineNumber].length; i++) {

            Case caseActuelle = this.getCases()[lineNumber][i];
            String representationCaseActuelle = "";

            if (caseActuelle instanceof CaseVide) {
                representationCaseActuelle = "  *  ";
            } else {
                int nbDigits = String.valueOf(((CaseTuile)caseActuelle).getNombre()).length();
                switch (nbDigits){
                    case 1:
                        representationCaseActuelle = "  "+((CaseTuile) caseActuelle).getNombre() + "  ";
                        break;
                    case 2:
                        representationCaseActuelle = " "+((CaseTuile) caseActuelle).getNombre() + "  ";
                        break;
                    case 3:
                        representationCaseActuelle = " "+((CaseTuile) caseActuelle).getNombre() + " ";
                        break;
                    case 4:
                        representationCaseActuelle = " "+((CaseTuile) caseActuelle).getNombre() + "";
                        break;
                }
            }
            result += representationCaseActuelle;
        }


        return result;
    }



    public boolean isDeplacementEffectue() {
        return deplacementEffectue;
    }

    public void setDeplacementEffectue(boolean deplacementEffectue) {
        this.deplacementEffectue = deplacementEffectue;
    }


}
