package jeu;

import java.io.Serializable;
import java.util.ArrayList;

import model.Case;
import model.CaseTuile;
import model.Partie;


/**
 * Cette classe gére l'ensemble des grille du jeux et permet le déplacement intergrille et intragrille
 */
public class GestionJeux implements Serializable{

    /**
     * Code permettant d'identifier le sens du déplacement
     */
    public static final int HAUT = 1;
    public static final int BAS = 2;
    public static final int GAUCHE = 3;
    public static final int DROITE = 4;
    public static final int INTERGRIDGAUCHE = 5;
    public static final int INTERGRIDDROITE = 6;

    /**nouvelle case qui est créer par la methode creerTuileAleatoire */
    private Case nouvelleCase=null;
    
    //est ce que le jeu est en cours?
    private boolean jeuEnCours=false;

    /**
     * Correspond à la partie en cour
     */
    private Partie partie;


    /**
     * Permet de créer une instance de Gestionjeux
     */
    public GestionJeux(){
        this.partie = new Partie(3);
        for(int i = 0; i < 2; i++){
            this.creerTuileAleatoire();
        }
    }

    /**
     * Permet d'envoyer le déplacement à la partie
     * @param typeDeplacement
     */
    public void effectuerDeplacement(int typeDeplacement){
        if((GestionJeux.HAUT <= typeDeplacement) && (typeDeplacement <= GestionJeux.DROITE)){
            partie.deplacerCasesIntraGrille(typeDeplacement);
        }else{
            partie.deplacerCasesInterGrille(typeDeplacement);
        }
    }

    
    /**
     * method qui va permettre de faire passé l'attribut booleén "nouveau" des nouvelle case a faux, elle est appelé dans creerTuileAleatoire
     */
    private void setAllNewToOld() {
    	Case c= this.nouvelleCase;
    	if(c!=null) {
	    	if(c instanceof CaseTuile){
	    		if(((CaseTuile)c).getNouveau())((CaseTuile)c).utilise();
	    	}
    	}
    }
    
    /**
     * getteur de partie
     * @return
     */
    public Partie getPartie() {
        return partie;
    }

    /**
     * setteur de partie
     * @param partie
     */
    public void setPartie(Partie partie) {
        this.partie = partie;
    }
    

    /**
     * method qui créer une tuileAleatoire dans la partie
     * cette methode ne peux créer de tuile sur une tuile déja existante
     */
    public void creerTuileAleatoire(){
    	this.setAllNewToOld();
        if(this.partie.verifierDeplacement() || this.partie.getNbTours() <= 1  ){
            this.partie.setNbTours(this.getPartie().getNbTours()+1);
            ArrayList<int[]> coordTeste = new ArrayList<int[]>();
            int randomChoixGrille,randomX,randomY;
            int tentatives = 0;
            boolean trouve = false;

            int maxTentative = (this.getPartie().getGrilles().get(0).getCases().length * this.getPartie().getGrilles().get(0).getCases().length) * this.getPartie().getGrilles().size();


            //On génére le placement aléatoire

            while(!trouve && tentatives < maxTentative){
                randomChoixGrille=(int)(Math.random() * (this.partie.getGrilles().size()));
                randomX = (int)(Math.random() * (this.partie.getGrilles().get(0).getCases().length));
                randomY = (int)(Math.random() * (this.partie.getGrilles().get(0).getCases().length));
                int[] couple = {randomX,randomY};
                tentatives++;

                if(!this.getPartie().getGrilles().get(randomChoixGrille).isCaseAtCoord(randomY,randomX) && !(coordTeste.contains(couple)) ){
                    trouve = true;
                    //Générer un nombre 2 ou 4
                    int nombre = 1 + (int)(Math.random() * (100 - 1));
                    if(nombre <= 66){
                        nombre = 2;
                    }else{
                        nombre = 4;
                    }

                    this.getPartie().getGrilles().get(randomChoixGrille).ajouterTuile(new CaseTuile(randomX,randomY,nombre));
                    this.nouvelleCase=this.getPartie().getGrilles().get(randomChoixGrille).getCases()[randomY][randomX];
                }
                coordTeste.add(couple);
            }

        }

	}
    
    public void setJeuEnCours(boolean s) {
    	this.jeuEnCours=s;
    }
    
    public boolean getJeuEnCours() {
    	return this.jeuEnCours;
    }




}
