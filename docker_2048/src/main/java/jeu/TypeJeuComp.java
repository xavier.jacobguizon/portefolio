package jeu;

import controller.DeplacementStrategy;
import model.Partie;
import multijoueur.Client;
import multijoueur.Message;
import multijoueur.ServeurComp;
import view.GraphiqueStrategy;

/**
 * Représente un type de jeu Compétitf
 */
public class TypeJeuComp extends TypeJeu {
    /**
     * Instance du client
     */
    private Client client;
    /**
     * Partie de l'adversaire
     */
    private Partie partieAdversaire;
    /**
     * Serveur connecté
     */
    private ServeurComp serveur;
    /**
     * Permet la création du type de jeu compétitif
     * @param deplacementStrategy
     * @param graphiqueStrategy
     * @param gestionJeux
     */
    public TypeJeuComp(DeplacementStrategy deplacementStrategy, GraphiqueStrategy graphiqueStrategy, GestionJeux gestionJeux) {
        super(deplacementStrategy, graphiqueStrategy, gestionJeux);
        this.getGestionJeux().getPartie().setGamemode("COMP");
    }

    /**
     * Permet la création d'un serveur lors de l'hebergement d'une partie Coop
     *
     * @param nomServeur
     */
    public void hebergerUnePartie(String nomServeur) {
        this.serveur = new ServeurComp(nomServeur);
        System.out.println("Lancement du serveur en cours . . .");

        Thread serveurThread = new Thread(serveur);
        serveurThread.start();
        System.out.println("Port : "+this.serveur.getPort());
        this.client = new Client("0.0.0.0", this.serveur.getPort());
        Thread clientTh = new Thread(this.client);
        clientTh.start();
        this.client.envoyerPartie(this.getGestionJeux().getPartie());

        boolean partieRecupere = false;


        //Réception de la partie de l'autre joueur
        while (!partieRecupere) {
            this.getClient().demanderPartieAutreJoueur();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (this.getClient().isNouveauMessage()) {
                this.getClient().setNouveauMessage(false);
                if (this.getClient().getSujetDernierMessage() == Message.ENVOI_PARTIE) {
                    partieRecupere = true;
                    this.setPartieAdversaire(this.getClient().getPartieRecu());
                    this.getClient().setReady();
                }
            }
        }
    }

    /**
     * Permet de rejoindre une partie
     *
     * @param host
     * @param port
     */
    public void rejoindreUnePartie(String host, int port) {
        this.client = new Client(host, port);
        Thread clientTh = new Thread(this.client);
        clientTh.start();
        //On doit récupérer la partie distante
        System.out.println("Connexion en cours . . .");
        System.out.println("Récupération de la partie distante . . .");
        boolean partieRecupere = false;

        while (!partieRecupere) {
            this.getClient().demanderPartieAutreJoueur();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (this.getClient().isNouveauMessage()) {
                this.getClient().setNouveauMessage(false);
                if (this.getClient().getSujetDernierMessage() == Message.ENVOI_PARTIE) {
                    partieRecupere = true;
                    this.setPartieAdversaire(this.getClient().getPartieRecu());
                    this.getClient().setReady();
                    // Envoie de sa partie à réflechir
                    this.getClient().envoyerPartie(this.getGestionJeux().getPartie());

                }
            }
        }
        System.out.println("Partie récupéré avec succés !");

    }

    /**
     * Permet la gestion des modéles pour ce type de jeu
     */
    @Override
    public void update() {
        //Si on à reçu une partie on la récupére et on demande une synchro de l'affichage
        if(this.getClient().isNouveauMessage() && this.getClient().getSujetDernierMessage() == Message.ENVOI_PARTIE){
            this.partieAdversaire = this.getClient().getPartieRecu();
            this.getClient().setNouveauMessage(false);
        }

        //Demande du déplacement
        if (this.getDeplacementStrategy() != null) {
            this.getDeplacementStrategy().choisirProchainDeplacement();
        }

        //Si il y a eu un déplacement alors on envoie la partie au serveur et on vérifie si on a gagné ou perdu
        if (this.getGestionJeux().getPartie().isModif()) {
            this.getGestionJeux().getPartie().setModif(false);
            //Envoie de la partie
            this.client.envoyerPartie(this.getGestionJeux().getPartie());

            //Vérification de l'état de la partie
            if(this.getGestionJeux().getPartie().verifierSiTermine()){
                //La partie est terminé
                if(this.getGestionJeux().getPartie().isPartieGagne()){
                    //Le joueur a gagné
                    this.getClient().envoyerFinDePartie(true);
                    this.getGraphiqueStrategy().finDePartie(true);
                }else if(this.getGestionJeux().getPartie().isPartiePerdu()){
                    //Le joueur a perdu
                    this.getClient().envoyerFinDePartie(false);
                    this.getGraphiqueStrategy().finDePartie(false);
                }
                this.getJeu().terminerLeJeu();
            }
        }

        //On vérifie si l'autre joueur à terminé la partie
        if(this.getClient().isNouveauMessage()){
            if(this.getClient().getSujetDernierMessage() == Message.END_WIN){
                //L'autre joueur à perdu
                this.getGraphiqueStrategy().finDePartie(true);
                this.getJeu().terminerLeJeu();

            }else if(this.getClient().getSujetDernierMessage() == Message.END_LOSE){
                //L'autre joueur à gagné
                this.getGraphiqueStrategy().finDePartie(false);
                this.getJeu().terminerLeJeu();

            }
        }

    }

    /**
     * Permet la gestion de l'affichage pour ce type de jeu
     */
    @Override
    public void display() {
        try{
            System.out.println("Score adversaire : "+this.partieAdversaire.getJoueur().getScore());
        }catch (Exception e){
            System.out.println("Score adversaire : 0");
        }

       /* if(this.getClient().isNouveauMessage() && this.getClient().getSujetDernierMessage() == Message.END_LOSE){
            this.getClient().setNouveauMessage(false);
            this.getGraphiqueStrategy().finDePartie(false);
        }else if(this.getClient().isNouveauMessage() && this.getClient().getSujetDernierMessage() == Message.END_WIN){
            this.getClient().setNouveauMessage(false);
            this.getGraphiqueStrategy().finDePartie(true);
        }else{*/
            this.getGraphiqueStrategy().update();
        //}
    }

    /**
     * Permet de préparer l'environnement de jeu pour le type de jeu compétitif pour le mode graphique
     * @param ds
     */
    @Override
    public void preparerEnvironnementDeJeu(DeplacementStrategy ds) {

    }

    /**
     * Permet de préparer l'environnement de jeu pour le type de jeu compétitif pour le mode console
     */
    @Override
    public void preparerEnvironnementDeJeu() {
        this.getGraphiqueStrategy().initGraphics(this.getGestionJeux().getPartie());
        this.getDeplacementStrategy().initModele(this.getGestionJeux());
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Partie getPartieAdversaire() {
        return partieAdversaire;
    }

    public void setPartieAdversaire(Partie partieAdversaire) {
        this.partieAdversaire = partieAdversaire;
    }
}
