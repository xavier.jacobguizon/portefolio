package jeu;

import model.Jeu;
import view.GraphiqueStrategy;

/**
 * Cette classe gére le changement d'un menu à un autre
 */
public class Menu {
    /**
     * Différents codes permettant d'identifier le menu cible
     */
    public static final int MENU_PRINCIPAL = 0;
    public static final int MENU_MULTIJOUEUR_HOST_JOIN = 1;
    public static final int MENU_MULTIJOUEUR_HOST_COMP_COOP = 2;
    public static final int MENU_MULTIJOUEUR_HOST_LOBBY = 3;
    public static final int MENU_MULTIJOUEUR_JOIN_SERVERLIST = 4;
    public static final int MENU_MULTIJOUEUR_JOIN_COMP_COOP = 5;
    public static final int MENU_MULTIJOUEUR_HOST_SERVER_NAME = 6;
    public static final int MENU_PSEUDO = 7;
    public static final int MENU_SCOREBOARD = 8;

    /**
     * Instance du jeu
     */
    private Jeu jeu;
    /**
     * Le type graphique utilisé
     */
    private GraphiqueStrategy graphiqueStrategy;

    /**
     * Permet l'instanciation de la classe Menu
     * @param graphiqueStrategy
     */
    public Menu(GraphiqueStrategy graphiqueStrategy){
        this.graphiqueStrategy = graphiqueStrategy;
    }


    /**
     * Permet d'accéder à un menu spécifique
     * @param typeMenu
     * @throws Exception
     */
    public void accederAuMenu(int typeMenu) throws Exception {
        if(typeMenu>=0 && typeMenu<=8){
            switch (typeMenu){
                case MENU_PRINCIPAL:
                    graphiqueStrategy.menuChoixSoloMultijoueur(this.jeu);
                    break;
                case MENU_MULTIJOUEUR_HOST_JOIN:
                    graphiqueStrategy.menuSubMultiJoueurCreeRejoindre(this.jeu);
                    break;
                case MENU_MULTIJOUEUR_HOST_COMP_COOP:
                    graphiqueStrategy.menuSubCreationCompCoop(this.jeu);
                    break;
                case MENU_MULTIJOUEUR_HOST_LOBBY:
                    graphiqueStrategy.menuLobby(this.jeu);
                    break;
                case MENU_MULTIJOUEUR_JOIN_SERVERLIST:
                    graphiqueStrategy.menuSubRejoindrePartie(this.jeu);
                    break;
                case MENU_MULTIJOUEUR_JOIN_COMP_COOP:
                    graphiqueStrategy.menuJoinCompCoop(this.jeu);
                    break;
                case MENU_MULTIJOUEUR_HOST_SERVER_NAME:
                    graphiqueStrategy.menuServerName(this.jeu);
                    break;
                case MENU_PSEUDO:
                    graphiqueStrategy.menuPseudo(this.jeu);
                    break;
                case MENU_SCOREBOARD:
                    graphiqueStrategy.menuScoreboard(this.jeu);
                    break;
            }
        }else{
            throw new Exception("Aucun menu ne correspond à celui qui est demandé !");
        }
    }


    public Jeu getJeu() {
        return jeu;
    }

    public void setJeu(Jeu jeu) {
        this.jeu = jeu;
    }

    public GraphiqueStrategy getGraphiqueStrategy() {
        return graphiqueStrategy;
    }

    public void setGraphiqueStrategy(GraphiqueStrategy graphiqueStrategy) {
        this.graphiqueStrategy = graphiqueStrategy;
    }
}
