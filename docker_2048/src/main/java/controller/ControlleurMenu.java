package controller;

import java.io.IOException;

import model.Jeu;
import model.JeuGraphique;
import view.AffichageInterface;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import jeu.Menu;
import jeu.TypeJeuSolo;


/**
 * Controlleur utilisé pour le Menu principal de l'interface graphique. 
 * 
 */
public class ControlleurMenu extends ControlleurAbstraitChangementFenetre{
	
	// Boutons du menu
    private @FXML Button buttonSolo;    //Pour le jeu en solo.
    private @FXML Button buttonCharge;  //Pour charger une partie.
    private @FXML Button buttonMulti;   //Pour jouer en multijoueur.
    private @FXML Button buttonTheme;   //Pour changer le thème des tuiles.
    private @FXML Button Host;			//Pour héberger une partie.
    private @FXML Button Join;			//Pour rejoindre une partie.
    private @FXML Button buttonRetour; // pour revenir sur le menu principal
    private @FXML TextArea pseudo;    //pour entrer son pseudo
    private @FXML Button validePseudo; //pour valider l'entrer
    private @FXML TextField casePseudo; //pour afficher le pseudo
    
    
	private Jeu jeu;
	
	/**
	 * Méthode qui permet de traiter le mode compétitif.
	 * 
	 * @param e
	 * @throws IOException
	 */
	public void ChoixCompet(ActionEvent e) throws IOException {
		this.jeu.setCoop(false);
		((AffichageInterface)this.jeu.getGraphiqueStrategy()).setEventControlleur(e,this);
		
		if(this.jeu.isHost()) {
			this.jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_SERVER_NAME);
			
		}else {
			this.jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_JOIN_SERVERLIST);	
		}		
	}
	
	/**
	 * Méthode qui permet de traiter le mode coopératif.
	 * 
	 * @param e
	 * @throws IOException
	 */
	public void ChoixCoop(ActionEvent e) throws IOException {
		this.jeu.setCoop(true);
		((AffichageInterface)this.jeu.getGraphiqueStrategy()).setEventControlleur(e,this);
		if(this.jeu.isHost()) {
			this.jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_SERVER_NAME);
		}else {
			this.jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_JOIN_SERVERLIST);
		}
		
		
	}
	
	/**
	 * Méthode qui envoie l'utilisateur sur un menu pour demander si il veut la coopération 
	 * ou la compétition (on garde en mémoire qu'on veux host).
	 * 
	 * @param e
	 * @throws IOException
	 */
	public void Host(ActionEvent e) throws IOException {
		((AffichageInterface)this.jeu.getGraphiqueStrategy()).setEventControlleur(e,this);
		this.jeu.setHost(true);
		this.jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_COMP_COOP);
		
	}
	
	/**
	 * method qui envoie l'utilisateur sur un menu pour demander 
	 * si il veut la coopération ou la compétition (on garde en mémoire qu'on veux Join)
	 * @param e
	 * @throws IOException
	 */
	public void Join(ActionEvent e) throws IOException {
		//this.jeu.setTypeJeu(new TypeJeuSolo(jeu.getDeplacementStrategy(),jeu.getGraphiqueStrategy(),jeu.getGestionJeux()));
		((AffichageInterface)this.jeu.getGraphiqueStrategy()).setEventControlleur(e,this);
		this.jeu.setHost(false);
		
		this.jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_JOIN_COMP_COOP);
		
		
	}
	
	/**
	 * choix de jouer en solo par l'utilisateur, préparation et lancement d'un jeu
	 * @param e
	 * @throws IOException
	 */
	public void buttonSolo(ActionEvent e) throws IOException {
	
		//mis en place d'un type de jeu
		this.jeu.setTypeJeu(new TypeJeuSolo(jeu.getDeplacementStrategy(),jeu.getGraphiqueStrategy(),jeu.getGestionJeux()));
		this.jeu.getTypeJeu().preparerEnvironnementDeJeu(ChangementFxml(this,"InterfaceGraphique.fxml",e,this.jeu.getTheme()));

		try {
			jeu.lancerJeu();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	}
	
	/**
	 * Méthode qui permet le chargement d'une partie préalablement sauvegardée.
	 * 
	 * @param e
	 * @throws IOException
	 */
	public void buttonCharge(ActionEvent e) throws IOException{
		//récupération de l'action si on charge un ancien jeu
		((JeuGraphique)this.jeu).chargerPartie(ChangementFxml(this,"InterfaceGraphique.fxml",e,jeu.getTheme()));
	}
	
	/**
	 * choix de jouer en multijoueur par l'utilisateur, redirection vers d'autre menu
	 * @param e
	 * @throws IOException
	 */
	public void buttonMulti(ActionEvent e) throws IOException{
		((AffichageInterface)this.jeu.getGraphiqueStrategy()).setEventControlleur(e,this);
		
		this.jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_JOIN);
		//ChangementFxml(this,"MenuHostJoin.fxml",e,this.jeu.getTheme());
        
    	
	}
	
	
	/**
	 * Méthode qui permet l'affichage de l'overlay pour le changement de thème.
	 * @param e
	 * @throws IOException
	 */
	public void buttonTheme(ActionEvent e) throws IOException{
		Stage primaryStage = (Stage) buttonSolo.getScene().getWindow();
		Stage stage = new Stage(); 	
		stage.setTitle("Changer le thème");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/MenuTheme.fxml"));   
		Parent root = loader.load();
		ControlleurTheme ct = loader.getController();
		ct.recupJeu(this.jeu);
		Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setX(primaryStage.getX() + 600);
        stage.setY(primaryStage.getY() + 70);
        stage.show();
	}
	
	/**
	 * method qui permet de revenir au menu principal a partir du bouton retour des différente interfaces du multijoueur
	 * @param e
	 * @throws IOException
	 */
	public void retour(ActionEvent e) throws IOException{
		
		((AffichageInterface)this.jeu.getGraphiqueStrategy()).setEventControlleur(e,this);
		this.jeu.changerDeMenu(Menu.MENU_PRINCIPAL);

	}
	
	/**
	 * methode qui va analyser les inputs du textArea de notre choix de pseudo, si on appuie sur entré, alors le choix est sauvegardé
	 * @param e
	 */
	public void pseudo(KeyEvent e) {
		
		if(e.getCode().equals(KeyCode.ENTER)) {
			ActionEvent t=new ActionEvent(e.getSource(), Host);	
			
			String pseudoJoueur="";
			for(int i=0;i<this.pseudo.getText().toCharArray().length-1;i++) {
				pseudoJoueur+=this.pseudo.getText().toCharArray()[i];
			}
			this.jeu.getGestionJeux().getPartie().getJoueur().setPseudo(pseudoJoueur);
			ValidePseudo(t);
			
		}
		
		
		
	}
	
	/**
	 * method qui validera le pseudo et permet d'aller sur le menu de choix
	 * il sauvegardera le pseudo
	 * @param e
	 */
	public void ValidePseudo(ActionEvent e) {
		//on supprime le retour chariot	
		this.jeu.getGestionJeux().getPartie().getJoueur().setPseudo(this.pseudo.getText());
		
		((AffichageInterface)this.jeu.getGraphiqueStrategy()).setEventControlleur(e,this);
		this.jeu.changerDeMenu(Menu.MENU_PRINCIPAL);
	}
	
	/**
	 * Setter de l'attribut jeu.
	 * 
	 * @param j
	 */
	public void setJeu(Jeu j) {
		this.jeu=j;
	}
	
	/**
	 * Getter de l'attribut jeu.
	 * 
	 * @return Jeu
	 */
	public Jeu getJeu() {
		return this.jeu;
	}
	
	/**
	 * on donne au textfield le pseudo du joueur qu'on a sauvegardé
	 */
	public void setPseudo() {
		this.casePseudo.setText(this.jeu.getGestionJeux().getPartie().getJoueur().getPseudo());
	}
    

}
