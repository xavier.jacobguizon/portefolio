package controller;

import java.util.Scanner;

import jeu.GestionJeux;

/**
 * class qui controle le déplacement en console
 * @author Lucas
 *  Cette classe est le controleur permettant d'effectuer un déplacement dans le mode d'affichage console
 */
public class ControlleurDeplacementConsole implements DeplacementStrategy{

	/**
     * GestionJeux donnant accés à la possibilité de modifier le modéle
     */
    private GestionJeux gestionJeux;

    /**
     * method qui permet de choisir le prochain déplacement
     * en fonction de l'entrée on modifie GestionJeu afin d'organiser le déplacement
     * si l'entrée est valide, on procède au déplacement et on créer la prochaine tuile
     */
    public void choisirProchainDeplacement() {
        int choix=-1;
        boolean validInput = false;
        Scanner sc = new Scanner(System.in);
        System.out.println("Choix de votre prochain coup :");
        System.out.println("Z- Haut S- Bas Q- Gauche D- Droite | A- Grille Gauche E- Grille Droite");
        String selection = sc.next();
        selection= selection.toUpperCase();

        while(!validInput){
            selection= selection.toUpperCase();
            switch(selection){
                case "Z":
                    choix= GestionJeux.HAUT;
                    validInput = true;
                    break;
                case "S":
                    choix=GestionJeux.BAS;
                    validInput = true;
                    break;
                case "Q":
                    choix= GestionJeux.GAUCHE;
                    validInput = true;
                    break;
                case "D":
                    choix=GestionJeux.DROITE;
                    validInput = true;
                    break;
                case "A":
                    choix= GestionJeux.INTERGRIDGAUCHE;
                    validInput = true;
                    break;
                case "E":
                    choix=GestionJeux.INTERGRIDDROITE;
                    validInput = true;
                    break;
            }
            if(!validInput){
                System.out.println("Veuillez sélectionner une option valide !");
                System.out.println("Z- Haut S- Bas Q- Gauche D- Droite | A- Grille Gauche E- Grille Droite");
                selection = sc.next();
            }
        }
        
        if(validInput) {
            System.out.println("Input valide");
        	 this.gestionJeux.effectuerDeplacement(choix);
            this.gestionJeux.getPartie().setModif(true);
            this.gestionJeux.creerTuileAleatoire();
        }
    }

    /**
     * Permet d'initialiser le modele GestionJeux
     * @param gestionJeux
     */
    @Override
    public void initModele(GestionJeux gestionJeux) {
        this.gestionJeux = gestionJeux;
    }
}
