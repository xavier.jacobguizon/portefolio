package controller;

import model.Jeu;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ControlleurTheme {
	
	Jeu jeu;
	
	private @FXML Button button2048;
    private @FXML Button buttonWindows;
    private @FXML Button buttonGlaciale;
    
    public void recupJeu(Jeu j) {
    	jeu = j;
    }
	
	public void changerTheme(ActionEvent e) {
		Object source = e.getSource();

		if (source == button2048) {
			jeu.setTheme(1);
		}
		if (source == buttonWindows) {
			jeu.setTheme(2);
		}
		if (source == buttonGlaciale) {
			jeu.setTheme(3);
		}
		
		Stage stage = (Stage) button2048.getScene().getWindow();
        stage.close();
	}
}
