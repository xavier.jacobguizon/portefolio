package controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * class abstraite qui controle le changement de fenetre et permet l'heritage 
 * @author blondin
 *
 */
public abstract class ControlleurAbstraitChangementFenetre {
	
	/**
	 * Méthode chargée du changement d'interface.
	 * 
	 * @param controlleurDeplacementJoueur
	 * @param url
	 * @param event
	 * @return
	 * @throws IOException
	 */
	public DeplacementStrategy ChangementFxml(ControlleurDeplacementJoueur controlleurDeplacementJoueur, String url,ActionEvent event)throws IOException{
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/"+url));

    	Parent root=loader.load();
    	
    	if(loader.getController() instanceof ControlleurDeplacementJoueur) {
    		((ControlleurDeplacementJoueur)loader.getController()).setJeu(controlleurDeplacementJoueur.getJeu());
    	}else {
    		//((ControlleurMenu)loader.getController()).setJeu(controlleurDeplacementJoueur.getJeu());
    	    root.setOnKeyPressed(loader.getController());
    	}
    	
    	
       Scene scene = new Scene(root);
       Stage stage=(Stage)((Node) event.getSource()).getScene().getWindow();

       stage.setScene(scene);
       stage.show();
    	
      
		
       if(loader.getController() instanceof ControlleurDeplacementJoueur) {
    	   return null;
       }else {
    	   return loader.getController();
       }
    }
	
	
	
	/**
	 * Méthode chargée du changement d'interface avec possibilité de changer de thème. 
	 * 
	 * @param cm
	 * @param url
	 * @param event
	 * @param choixTheme
	 * @return
	 * @throws IOException
	 */
    public DeplacementStrategy ChangementFxml(ControlleurMenu cm, String url,ActionEvent event,int choixTheme)throws IOException{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/"+url));
    	Parent root=loader.load();
    	
    	if(loader.getController() instanceof ControlleurMenu) {
    		((ControlleurMenu)loader.getController()).setJeu(cm.getJeu());
    	}else {
    	   root.setOnKeyPressed(loader.getController());
    	}
    	
    	
       Scene scene = new Scene(root);
       Stage stage=(Stage)((Node) event.getSource()).getScene().getWindow();

	   System.out.println(event.getSource());
	   Scene test = ((Node) event.getSource()).getScene();
	   System.out.println(test);
	   System.out.println(test.getWindow());
	   System.out.println((Stage)test.getWindow());

	   //if(stage == null) {
		//	System.out.println(event.getSource());
		//	System.out.println((Node) event.getSource());
			
			//System.out.println(((Node) event.getSource()).getScene().getWindow());
	   //}

       stage.setScene(scene);
       stage.show();
    	
       //ajout du css pour utilisation dans ControlleurDeplacementJoueur
       switch (choixTheme) {
       case 1:
    	   stage.getScene().getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
    	   break;
       case 2:
    	   stage.getScene().getStylesheets().add(getClass().getResource("/css/stylesWindows.css").toExternalForm());
    	   break;
       case 3:
    	   stage.getScene().getStylesheets().add(getClass().getResource("/css/stylesGlaciale.css").toExternalForm());
    	   break;
       default :
    	   stage.getScene().getStylesheets().add(getClass().getResource("/css/styles.css").toExternalForm());
    	   break;
       }
    	
		
       if(loader.getController() instanceof ControlleurMenu) {
    	   return null;
       }else {
    	   return loader.getController();
       }
    }

}
