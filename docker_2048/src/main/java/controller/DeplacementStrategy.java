package controller;

import jeu.GestionJeux;

/**
 * Design Pattern Strategy pour le déplacement
 */
public interface DeplacementStrategy {
    /**
     * Permet au joueur de choisir son prochain déplacement
     */
    public void choisirProchainDeplacement();

    /**
     * Permet d'initialiser le modéle
     * @param gestionJeux
     */
    public void initModele(GestionJeux gestionJeux);
}
