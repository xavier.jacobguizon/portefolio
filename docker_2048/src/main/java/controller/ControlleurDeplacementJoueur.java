package controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.Jeu;
import model.JeuGraphique;
import view.AffichageInterface;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import jeu.GestionJeux;
import jeu.Menu;


/**
 * Controlleur utilisé pour gérer le jeu (déplacement du joueur, sauvegarde, score, ect...) via une interface graphique.
 *
 */
public class ControlleurDeplacementJoueur implements EventHandler<KeyEvent>, controller.DeplacementStrategy{

	// Attribut supplémentaires pour le changment d'interface, la sauvegarde et le chargement d'une partie.
	private Jeu jeu;
	private GestionJeux gestionJeux;
	private boolean demandeDeplacement = true;

	// Bouton de la prochaine action pour l'I.A
	private @FXML Button prochaineAction;

	// Boutons du pad de déplacement sur l'interface graphique.
	private @FXML Button haut;
	private @FXML Button gauche;
	private @FXML Button droite;
	private @FXML Button bas;
	private @FXML Button intergauche;
	private @FXML Button interdroite;

	// Boutons du menu option de l'interface graphique.
	private @FXML MenuItem sauvegarder;
	private @FXML MenuItem retour;
	private @FXML MenuItem quitter;

	// Élements de notre vue de l'interface graphique qui nous permet d'accéder aux différents "étages" de l'architecture.
	private @FXML Group grille;
	private @FXML Pane fond;
	private @FXML AnchorPane AnchorPane;
	private @FXML BorderPane parentActionView;


	/**
	 * Méthode Listener chargée de gérer les actions des boutons en fonction de leur "fx-id". 
	 * 
	 * @param e
	 */
	public void onAction(ActionEvent e) {
		Object source = e.getSource();

		// Ici on gère la sauvegarde de la partie.
		if (source == sauvegarder) {
			System.out.println(this.gestionJeux);
			ObjectOutputStream oos = null;
			try {
				final FileOutputStream fichier = new FileOutputStream("save.ser");
				oos = new ObjectOutputStream(fichier);
				oos.writeObject(this.gestionJeux);
				oos.flush();
			} catch (final IOException e1) {
				e1.printStackTrace();
			} finally {
				try {
					if (oos != null) {
						oos.flush();
						oos.close();
					}
				} catch (final IOException e2) {
					e2.printStackTrace();
				}
			}
			System.out.println("Partie sauvegardée dans save.ser à la racine du projet");
		}

		// Ici on gère le retour au menu principal.
		if (source == retour) {
				JeuGraphique.getJeuGraphique().changerDeMenu(Menu.MENU_PRINCIPAL);

		}

		// Ici on gère la fermeture de l'application.
		if (source == quitter) {

			int scoreJoueur = this.gestionJeux.getPartie().getJoueur().getScore();

			ObjectInputStream ois = null;
			try {
				final FileInputStream fichierIn = new FileInputStream("score.ser");
				ois = new ObjectInputStream(fichierIn);
				try {
					int mscore = (int) ois.readObject();
					if (scoreJoueur>mscore) {
						ObjectOutputStream oos = null;
						try {
							final FileOutputStream fichier = new FileOutputStream("score.ser");
							oos = new ObjectOutputStream(fichier);
							oos.writeObject(scoreJoueur);
							oos.flush();
						} catch (final IOException e1) {
							e1.printStackTrace();
						} finally {
							try {
								if (oos != null) {
									oos.flush();
									oos.close();
								}
							} catch (final IOException e2) {
								e2.printStackTrace();
							}
						}
						System.out.println("Score sauvegardée dans score.ser à la racine du projet");
					}
				} catch (ClassNotFoundException ex1) {
					// TODO Auto-generated catch block
					ex1.printStackTrace();
				}
			} catch (IOException ex2) {
				//e.printStackTrace();
				System.out.println("Fichier introuvable");
			}

			// On ferme l'application.
			Stage stage = (Stage) prochaineAction.getScene().getWindow();
			stage.close();
		}

	}

	@Override
	/**
	 * Méthode Listener chargée de prendre en compte les déplacements du joueur avec le clavier.
	 * 
	 * @param event
	 */
	public void handle(KeyEvent event){

		if(this.gestionJeux.getJeuEnCours()) {
			if(event.getEventType().equals(KeyEvent.KEY_PRESSED)) {
				System.out.println(this.gestionJeux.getPartie().isDemandeDeplacement());
				/*
		          Le joueur déplace toute les cases vers une direction pour chaque grille (déplacement intra-grille)
				 */

				if(this.gestionJeux.getPartie().isDemandeDeplacement()){

					if (event.getCode() == KeyCode.Z) {
						deplacement(GestionJeux.HAUT);
					} else if (event.getCode() == KeyCode.S) {
						deplacement(GestionJeux.BAS);
					} else if (event.getCode() == KeyCode.Q) {
						deplacement(GestionJeux.GAUCHE);
					} else if (event.getCode() == KeyCode.D) {
						deplacement(GestionJeux.DROITE);
					} else if (event.getCode() == KeyCode.E) {
						deplacement(GestionJeux.INTERGRIDDROITE);
					} else if (event.getCode() == KeyCode.A) {
						deplacement(GestionJeux.INTERGRIDGAUCHE);

					}
					//this.gestionJeux.getPartie().setDemandeDeplacement(false);
				}

			}
		}	

	}



	/**
	 * Méthode Listener chargée de prendre en compte les déplacements du joueur avec la souris.
	 * 
	 * @param event
	 */
	public void mouseHandle(ActionEvent event){
		Object source = event.getSource();
		if(this.gestionJeux.getJeuEnCours()) {    		
			if(this.gestionJeux.getPartie().isDemandeDeplacement()){
				if (source == haut) {
					deplacement(GestionJeux.HAUT);
				} else if (source == bas) {
					deplacement(GestionJeux.BAS);
				} else if (source == gauche) {
					deplacement(GestionJeux.GAUCHE);
				} else if (source == droite) {
					deplacement(GestionJeux.DROITE);
				} else if (source == interdroite) {
					deplacement(GestionJeux.INTERGRIDDROITE);
				} else if (source == intergauche) {
					deplacement(GestionJeux.INTERGRIDGAUCHE);
				}
			}
		}	

	}


	/*
	 * Méthode qui prend en charge d'appliquer le déplacmeent dans le modèle et d'aouter une tuile.
	 * 
	 * @param dep
	 */
	public void deplacement(int dep) {
		this.gestionJeux.getPartie().setModif(true);
		this.gestionJeux.effectuerDeplacement(dep);
		this.gestionJeux.creerTuileAleatoire();
	}

	/**
	 * Getter de l'attribut Jeu.
	 * 
	 * @return Jeu
	 */
	public Jeu getJeu() {
		return jeu;
	}

	/**
	 * Setter de l'attribut Jeu.
	 * 
	 * @param jeu
	 */
	public void setJeu(Jeu jeu) {
		this.jeu = jeu;
	}

	/**
	 * Getter de l'attribut GestionJeux.
	 * 
	 * @return gestionJeux
	 */
	public GestionJeux getGestionJeux() {
		return gestionJeux;
	}

	/**
	 * Setter de l'attribut GestionJeux.
	 * 
	 * @param gestionJeux
	 */
	public void setGestionJeux(GestionJeux gestionJeux) {
		this.gestionJeux = gestionJeux;
	}

	/**
	 * Méthode qui demande si un déplacement est demandé.
	 * 
	 * @return boolean
	 */
	public boolean isDemandeDeplacement() {
		return this.demandeDeplacement;
	}

	/**
	 * Permet d'initialiser le modèle pour ce controlleur.
	 *
	 * @param gestionJeux
	 */
	@Override
	public void initModele(GestionJeux gestionJeux) {
		System.out.println("deplacementJoueur : " + gestionJeux);
		if (this.gestionJeux != null) {
			throw new IllegalStateException("Le modèle peut être initialisé qu'une seule fois");
		}
		this.gestionJeux = gestionJeux;
		//Xavier j'ai déplacer ton code de la méthode start ici
		//this.gestionJeux.creerTuileAleatoire();
		AffichageInterface.getInstance().setTailleMax(parentActionView.getWidth(),parentActionView.getHeight());
		grille=AffichageInterface.getInstance();
		parentActionView.getChildren().remove(0);
		parentActionView.getChildren().add(grille);

	}

	@Override
	/**
	 * Méthode qui permet de choisir le prochain déplacement.
	 */
	public void choisirProchainDeplacement() {
		//if(this.gestionJeux!=null)
		//this.gestionJeux.getPartie().setDemandeDeplacement(true);
		//this.demandeDeplacement = true;

	}





















	/*
    	Scene scene = start.getScene();
    	boolean add = scene.getStylesheets().add("css/styles.css");



    	// utilisation de styles pour la grille et la tuile (voir styles.css)
        p.getStyleClass().add("pane"); 
        c.getStyleClass().add("tuile");


        grille.getStyleClass().add("gridpane");
        GridPane.setHalignment(c, HPos.CENTER);
        fond.getChildren().add(p);
        p.getChildren().add(c);

        // on place la tuile en précisant les coordonnées (x,y) du coin supérieur gauche
        p.setLayoutX(x);
        p.setLayoutY(y);

        p.setVisible(true);
        c.setVisible(true);

	 *


    /**
	 * Capture les événements clavier et permet de savoir dans quelle direction le joueur souhaite déplacer les cases
	 *
	 * @param event
	 *//*
    @Override
    public void handle(KeyEvent event){
    	if(event.getEventType().equals(event.KEY_PRESSED)) {
    		System.out.println("code : "+event.getCode());
	    	/*
	          Le joueur déplace toute les cases vers une direction pour chaque grille (déplacement intra-grille)
	  *//*
	        if(isDemandeDeplacement()){
	        	if (event.getCode() == KeyCode.Z) {
	                this.gestionJeux.effectuerDeplacement(GestionJeux.HAUT);
	            } else if (event.getCode() == KeyCode.S) {
	                this.gestionJeux.effectuerDeplacement(GestionJeux.BAS);
	            } else if (event.getCode() == KeyCode.Q) {
	                this.gestionJeux.effectuerDeplacement(GestionJeux.GAUCHE);
	            } else if (event.getCode() == KeyCode.D) {
	                this.gestionJeux.effectuerDeplacement(GestionJeux.DROITE);
	            } else if (event.getCode() == KeyCode.E) {
	                this.gestionJeux.effectuerDeplacement(GestionJeux.INTERGRIDDROITE);
	            } else if (event.getCode() == KeyCode.A) {
	                this.gestionJeux.effectuerDeplacement(GestionJeux.INTERGRIDGAUCHE);
	            }
	            //Le coup est joué, on ne demande plus de déplacement
	            setDemandeDeplacement(false);
	        }
    	}

        String touche = event.getText();



        try {
			updatePosition(touche);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        /*Task task = new Task<Void>() { // on définit une tâche parallèle pour mettre à jour la vue
            @Override
            public Void call() throws InterruptedException throws Exception { // implémentation de la méthode protected abstract V call() dans la classe Task
            	updatePosition(touche);
            	return null; // la méthode call doit obligatoirement retourner un objet. Ici on n'a rien de particulier à retourner. Du coup, on utilise le type Void (avec un V majuscule) : c'est un type spécial en Java auquel on ne peut assigner que la valeur null
            } // end call

        };
        ;
        Thread th = new Thread(task); // on crée un contrôleur de Thread
        th.setDaemon(true); // le Thread s'exécutera en arrière-plan (démon informatique)
        th.start(); // et on exécute le Thread pour mettre à jour la vue (déplacement continu de la tuile horizontalement)
	   */
	//}
	/*
    public synchronized void updatePosition(String touche) throws InterruptedException  {

    	if (touche.compareTo("q") == 0) { // utilisateur appuie sur "q" pour envoyer la tuile vers la gauche
            if (objectifx > 25) { // possible uniquement si on est pas dans la colonne la plus à gauche
                objectifx -= (int) 300 ; // on définit la position que devra atteindre la tuile en abscisse (modèle). Le thread se chargera de mettre la vue à jour
                score.setText(Integer.toString(Integer.parseInt(score.getText()) + 1)); // mise à jour du compteur de mouvement

            }
        }
        if (touche.compareTo("d") == 0) { // utilisateur appuie sur "d" pour envoyer la tuile vers la droite
            if (objectifx < (int) 445 - 2 * 397 / 4 - 24) { // possible uniquement si on est pas dans la colonne la plus à droite (taille de la fenêtre - 2*taille d'une case - taille entre la grille et le bord de la fenêtre)
                objectifx += (int) 300 ;
                score.setText(Integer.toString(Integer.parseInt(score.getText()) + 1));
            }
        }
        if (touche.compareTo("s") == 0){
        	System.out.println("S");
        	if (objectify < (int) 813 - 2 * 397 / 4 - 191) {
        		System.out.println("YES");
        		objectify += (int) 300 ;
        		score.setText(Integer.toString(Integer.parseInt(score.getText()) + 1));
        	}
        }
        if (touche.compareTo("z") == 0) {
        	if (objectify > (int) 191) {
        		objectify -= (int) 300 ;
        		score.setText(Integer.toString(Integer.parseInt(score.getText()) + 1));
        	}
        }
        //System.out.println("objectifx=" + objectifx);
        //System.out.println("objectify=" + objectify);

        while (x != objectifx || y!= objectify) { // si la tuile n'est pas à la place qu'on souhaite attendre en abscisse
            if (x < objectifx) {
                x += 1; // si on va vers la droite, on modifie la position de la tuile pixel par pixel vers la droite
            } else if (x>objectifx){
                x -= 1; // si on va vers la gauche, idem en décrémentant la valeur de x
            }
            if (y < objectify) {
                y += 1; // si on va vers le bas, on modifie la position de la tuile pixel par pixel vers le bas
            } else if (y> objectify) {
                y -= 1; // si on va vers le haut, idem en décrémentant la valeur de y
            }
            // Platform.runLater est nécessaire en JavaFX car la GUI ne peut être modifiée que par le Thread courant, contrairement à Swing où on peut utiliser un autre Thread pour ça
            Platform.runLater(new Runnable() { // classe anonyme
                @Override
                public void run() {
                    //javaFX operations should go here
                    p.relocate(x, y); // on déplace la tuile d'un pixel sur la vue, on attend 5ms et on recommence jusqu'à atteindre l'objectif
                    p.setVisible(true);    
                }
            }
            );
            Thread.sleep(1);
        } // end while

    }

	 */

}
