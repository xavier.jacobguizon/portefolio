package view;

import bdd.JoueurDB;
import bdd.ScoreBoard;
import bdd.Serveurs;
import model.*;
import jeu.*;

import java.util.ArrayList;
import java.util.Scanner;

public class AffichageConsole implements GraphiqueStrategy {

    private Partie modele;
    private static AffichageConsole affichageConsole;

    private AffichageConsole() {
    }

    public static synchronized AffichageConsole getInstance() {
        if (affichageConsole == null) {
            affichageConsole = new AffichageConsole();
        }
        return affichageConsole;
    }

    public static AffichageConsole getAffichageConsole() {
        return affichageConsole;
    }

    public static void setAffichageConsole(AffichageConsole affichageConsole) {
        AffichageConsole.affichageConsole = affichageConsole;
    }

    @Override
    public void initGraphics(Partie partie) {
        this.modele = partie;
    }

    /**
     * Menu permettant de choisir entre le jeu solo ou multijoueur
     *
     * @param jeu
     */
    @Override
    public void menuChoixSoloMultijoueur(Jeu jeu) {
        System.out.println("|||||||||| 2048 ||||||||||");
        System.out.println("1. Jeu en Solo\n2. Jeu en multijoueur\n3. Scoreboard");
        Scanner sc = new Scanner(System.in);
        /*
        Vérifier la cohérence du choix
            */
        int choix = -5;

        int choixBorneInf = 1;
        int choixBorneSup = 3;

        while (choix < choixBorneInf || choix > choixBorneSup) {
            while (!sc.hasNextInt()) {
                System.out.println("Veuillez rentrer une réponse correct");
                sc.next();
            }
            choix = sc.nextInt();
        }

        switch (choix) {
            case 1:
                //Ici on choisit de démarrer une partie en solo, on instancie le type de jeu correspondant puis on lance le jeu
                jeu.setTypeJeu(new TypeJeuSolo(jeu.getDeplacementStrategy(), jeu.getGraphiqueStrategy(), jeu.getGestionJeux()));
                jeu.getTypeJeu().preparerEnvironnementDeJeu();

                try {
                    jeu.lancerJeu();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_JOIN);
                break;
            case 3:
                jeu.changerDeMenu(Menu.MENU_SCOREBOARD);
                break;
        }
    }


    /**
     * Menu permettant le choix entre rejoindre ou créer une partie
     *
     * @param jeu
     */
    @Override
    public void menuSubMultiJoueurCreeRejoindre(Jeu jeu) {
        System.out.println("|||||||||| 2048 ||||||||||");
        System.out.println("1. Créer une partie\n2. Rejoindre une partie");
        Scanner sc = new Scanner(System.in);
        /*
        Vérifier la cohérence du choix
            */
        int choix = -5;

        int choixBorneInf = 1;
        int choixBorneSup = 2;

        while (choix < choixBorneInf || choix > choixBorneSup) {
            while (!sc.hasNextInt()) {
                System.out.println("Veuillez rentrer une réponse correct");
                sc.next();
            }
            choix = sc.nextInt();
        }


        switch (choix) {
            case 1:
                jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_COMP_COOP);
                break;
            case 2:
                jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_JOIN_COMP_COOP);
                break;
        }
    }


    @Override
    public void menuSubCreationCompCoop(Jeu jeu) {
        System.out.println("|||||||||| 2048 ||||||||||");
        System.out.println("1. Compétition\n2. Coopération");
        Scanner sc = new Scanner(System.in);
        /*
        Vérifier la cohérence du choix
            */
        int choix = -5;

        int choixBorneInf = 1;
        int choixBorneSup = 2;

        while (choix < choixBorneInf || choix > choixBorneSup) {
            while (!sc.hasNextInt()) {
                System.out.println("Veuillez rentrer une réponse correct");
                sc.next();
            }
            choix = sc.nextInt();
        }


        switch (choix) {
            case 1:
                //Créer jeu compétitif
                jeu.setTypeJeu(new TypeJeuComp(jeu.getDeplacementStrategy(), jeu.getGraphiqueStrategy(), jeu.getGestionJeux()));
                jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_SERVER_NAME);
                break;
            case 2:
                //créer jeu coop
                jeu.setTypeJeu(new TypeJeuCoop(jeu.getDeplacementStrategy(), jeu.getGraphiqueStrategy(), jeu.getGestionJeux()));
                jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_SERVER_NAME);
                break;
        }
    }


    /**
     * Ce menu affiche la liste des serveurs disponible et permet d'en choisir un
     *
     * @param jeu
     */
    @Override
    public void menuSubRejoindrePartie(Jeu jeu) {
        //On affiche la liste des serveurs
        ArrayList<Serveurs> serveurs = Serveurs.getServeursList();
        System.out.println("|-|-|-| Liste des serveurs |-|-|-|");
        for (int i = 0; i < serveurs.size(); i++) {
            Serveurs serveur = serveurs.get(i);
            if(serveur.getGamemode().equals(this.modele.getGamemode())){
                System.out.println((i + 1) + "." + " " + serveur.getGamemode() + " " + serveur.getNom() + "  " + serveur.getNbJoueur() + " joueur(s)" + " Status :" + serveur.getStatus());
            }
        }

        int choixMin = 1;
        int choixMax = serveurs.size();

        Scanner sc = new Scanner(System.in);
        int choix = -5;

        while (choix < choixMin || choix > choixMax) {
            while (!sc.hasNextInt()) {
                System.out.println("Veuillez rentrer une réponse correct");
                sc.next();
            }
            choix = sc.nextInt();
        }

        Serveurs serveurChoisi = serveurs.get(choix - 1);

        //On rejoins une partie
        TypeJeu tj = jeu.getTypeJeu();
        if (tj instanceof TypeJeuCoop) {
            ((TypeJeuCoop) jeu.getTypeJeu()).rejoindreUnePartie(serveurChoisi.getHost(), serveurChoisi.getPort());
            jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_LOBBY);
        }else if(tj instanceof  TypeJeuComp){
            ((TypeJeuComp) jeu.getTypeJeu()).rejoindreUnePartie(serveurChoisi.getHost(), serveurChoisi.getPort());
            jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_LOBBY);
        }
    }


    /**
     * Affiche le lobby lorsque le joueur est en attente d'un autre
     *
     * @param jeu
     */
    @Override
    public void menuLobby(Jeu jeu) {
        System.out.println("|||||||||| 2048 ||||||||||");
        System.out.println("Lobby - En attente de joueur . . .");
        TypeJeu typejeu = jeu.getTypeJeu();

        if (typejeu instanceof TypeJeuCoop) {

            TypeJeuCoop typejeuCoop = (TypeJeuCoop) typejeu;

            //On attend que tout le monde soit connecter et prêt
            while (!typejeuCoop.getClient().isServerReady()) {
                typejeuCoop.getClient().demanderSiPret();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (typejeuCoop.getClient().isNouveauMessage()) {
                    System.out.println("En attente d'un partenaire !");
                    typejeuCoop.getClient().setNouveauMessage(false);
                }
            }
            System.out.println("Tout le monde est prêt, le jeu va démarrer !");

            try {
                jeu.lancerJeu();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(typejeu instanceof  TypeJeuComp){
            TypeJeuComp typejeuComp = (TypeJeuComp) typejeu;

            //On attend que tout le monde soit connecter et prêt
            while (!typejeuComp.getClient().isServerReady()) {
                typejeuComp.getClient().demanderSiPret();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (typejeuComp.getClient().isNouveauMessage()) {
                    System.out.println("En attente d'un partenaire !");
                    typejeuComp.getClient().setNouveauMessage(false);
                }
            }
            System.out.println("Tout le monde est prêt, le jeu va démarrer !");

            try {
                jeu.lancerJeu();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Menu permettant de choisir entre rejoindre une jeu Coop ou Comp
     *
     * @param jeu
     */
    @Override
    public void menuJoinCompCoop(Jeu jeu) {
        System.out.println("|||||||||| 2048 ||||||||||");
        System.out.println("|||||||| Rejoindre ||||||||");
        System.out.println("1. Compétition\n2. Coopération");
        Scanner sc = new Scanner(System.in);
        /*
        Vérifier la cohérence du choix
            */
        int choix = -5;

        int choixBorneInf = 1;
        int choixBorneSup = 2;

        while (choix < choixBorneInf || choix > choixBorneSup) {
            while (!sc.hasNextInt()) {
                System.out.println("Veuillez rentrer une réponse correct");
                sc.next();
            }
            choix = sc.nextInt();
        }


        switch (choix) {
            case 1:
                //Créer jeu compétitif
                jeu.setTypeJeu(new TypeJeuComp(jeu.getDeplacementStrategy(), jeu.getGraphiqueStrategy(), jeu.getGestionJeux()));
                this.modele = jeu.getGestionJeux().getPartie();
                jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_JOIN_SERVERLIST);
                break;
            case 2:
                //créer jeu coop
                jeu.setTypeJeu(new TypeJeuCoop(jeu.getDeplacementStrategy(), jeu.getGraphiqueStrategy(), jeu.getGestionJeux()));
                this.modele = jeu.getGestionJeux().getPartie();
                jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_JOIN_SERVERLIST);
                break;
        }
    
    jeu.getTypeJeu().preparerEnvironnementDeJeu();
    }

    /**
     * Menu permettant de rentrer le nom du serveur
     *
     * @param jeu
     */
    @Override
    public void menuServerName(Jeu jeu) {
        System.out.println("|||||||||| 2048 ||||||||||");
        System.out.println("Nom du serveur :");
        Scanner sc = new Scanner(System.in);
        String nom = sc.nextLine();

        while (nom.length() == 0) {
            while (!sc.hasNextLine()) {
                System.out.println("Veuillez rentrer une réponse correct");
                sc.nextLine();
            }
            nom = sc.nextLine();
        }

        if(jeu.getTypeJeu() instanceof  TypeJeuCoop){
            ((TypeJeuCoop) jeu.getTypeJeu()).hebergerUnePartie(nom);
        }else if(jeu.getTypeJeu() instanceof  TypeJeuComp){
            ((TypeJeuComp) jeu.getTypeJeu()).hebergerUnePartie(nom);
        }
        
        System.out.println("Preparation du lobby");
        jeu.getTypeJeu().preparerEnvironnementDeJeu();
        jeu.changerDeMenu(Menu.MENU_MULTIJOUEUR_HOST_LOBBY);
    }

    @Override
    public void menuPseudo(Jeu jeu) {
        
        String nom = "";
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("|||||||||| 2048 ||||||||||");
            System.out.println("Choisissez un pseudo :");

            nom = sc.nextLine();
            if (nom.isEmpty()) {
                System.out.println("Veuillez rentrer une réponse correct");
            }
        } while (nom.isEmpty());
        

        jeu.getGestionJeux().getPartie().getJoueur().setPseudo(nom);
        
        jeu.changerDeMenu(Menu.MENU_PRINCIPAL);
    }

    @Override
    public void menuScoreboard(Jeu jeu) {
        System.out.println("|||||||||| 2048 ||||||||||");
        System.out.println("Scoreboard");

        ScoreBoard sb = new ScoreBoard();
        ArrayList<JoueurDB> joueurs = sb.getScoreBoard();
        for(int i = 0; i < joueurs.size(); i++){
            JoueurDB joueur = joueurs.get(i);
            System.out.println(i+1+" | "+joueur.getPseudo()+" | "+joueur.getScore()+" | "+joueur.getTypeJeu());
        }


        System.out.println("Appuyez sur une touche pour continuer . . .");
        Scanner sc = new Scanner(System.in);
        //String nom = sc.nextLine();
            
        
        jeu.changerDeMenu(Menu.MENU_PRINCIPAL);
    }

    @Override
    public void update() {
        if (this.modele != null) {
            System.out.println(this.modele);
            this.modele.setModif(false);
        }else{
            System.out.println("Le modèle est null");
        }


    }

    @Override
    public void finDePartie(boolean gagne)  {
        if(gagne){
            System.out.println("Félicitation vous avez gagné !");
        }else{
            System.out.println("Dommage, vous avez perdu !");
        }
        for(int i = 0; i < 5; i++){
            System.out.println("Retour au menu dans "+(5-i)+" seconde(s)");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void updateStatus(Partie partie) {
        this.modele = partie;
        System.out.println(partie);
    }
}
