package view;

import model.Jeu;
import model.Partie;

public interface GraphiqueStrategy {
	public void initGraphics(Partie partie);

	/**
	 * Menu permettant de choisir entre le jeu Solo et Multijoueur
	 * @param jeu
	 */
	public void menuChoixSoloMultijoueur(Jeu jeu);

	/**
	 * Menu permettant de choisir entre créer ou rejoindre une partie en ligne
	 * @param jeu
	 */
	public void menuSubMultiJoueurCreeRejoindre(Jeu jeu);

	/**
	 * Menu permettant de choisir entre la création d'une partie en ligne Coop ou Comp
	 * @param jeu
	 */
	public void menuSubCreationCompCoop(Jeu jeu);

	/**
	 * Menu permettant de rejoindre une partie (affiche la liste des serveurs présent dans la BDD)
	 * @param jeu
	 */
	public void menuSubRejoindrePartie(Jeu jeu);

	/**
	 * Menu permettant l'affichage du lobby
	 * @param jeu
	 */
	public void menuLobby(Jeu jeu);

	/**
	 * Menu permettant de choisir entre rejoindre une partie Comp ou Coop
	 * @param jeu
	 */
	public void menuJoinCompCoop(Jeu jeu);

	/**
	 * Menu permettat de rentrer le nom du serveur
	 * @param Jeu
	 */
	public void menuServerName(Jeu Jeu);

	/**
	 * Permet de d'afficher le menu de sélection du pseudo
	 * @param jeu
	 */
	public void menuPseudo(Jeu jeu);

	/**
	 * Permet l'affichage du scoreboard
	 * @param jeu
	 */
	public void menuScoreboard(Jeu jeu);
	public void update();
	public void finDePartie(boolean gagne);
}
