package view;


import java.io.IOException;

import java.io.FileInputStream;

import java.io.ObjectInputStream;
//import java.util.ArrayList;

import controller.ControlleurMenu;
import model.Case;
import model.CaseTuile;
import model.Jeu;
import model.Partie;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import jeu.Grille;

public class AffichageInterface extends Group implements GraphiqueStrategy{

	public static final int TEMPSDEPLACEMENT = 5;
	
	private Partie modele;
	private static AffichageInterface affichageInterface;
	//private ThreadGroup groupTest=new ThreadGroup("group");
    private ActionEvent event;
    private ControlleurMenu controlleurFen;
	
	
	/**
	 * maxX est la taille maximum en x qu'AffichageInterface peux prendre dans la fenetre
	 * mis a jour par un setter
	 */
	private double maxX;
	
	/**
	 * maxX est la taille maximum en y qu'AffichageInterface peux prendre dans la fenetre
	 * mis a jour par un setter
	 */
	private double maxY;
	
	//private ArrayList<CaseTuile> lCaseTuilePrecedente=new ArrayList<CaseTuile>();
	
	private AffichageInterface(){ }

	public static synchronized AffichageInterface getInstance(){
		if(affichageInterface == null){
			affichageInterface = new AffichageInterface();
		}
		return affichageInterface;
	}
	
	
	
	@Override
	public void initGraphics(Partie partie) {
		this.modele = partie;
	}
	
	public void setEventControlleur(ActionEvent e,ControlleurMenu conFen) {
		this.event=e;
		this.controlleurFen=conFen;
	}
	
	@Override
	public void menuChoixSoloMultijoueur(Jeu jeu) {
		try {
			System.out.println(this.controlleurFen);
			System.out.println(event);
			System.out.println(jeu);
			this.controlleurFen.ChangementFxml(this.controlleurFen,"MenuPrincipal.fxml",event,jeu.getTheme());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void menuSubMultiJoueurCreeRejoindre(Jeu jeu) {
		try {
			System.out.println(this.controlleurFen);
			System.out.println(event);
					System.out.println(jeu);
			this.controlleurFen.ChangementFxml(this.controlleurFen,"MenuHostJoin.fxml",event,jeu.getTheme());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void menuSubCreationCompCoop(Jeu jeu) {
		try {
			this.controlleurFen.ChangementFxml(this.controlleurFen,"MenuCoopComp.fxml",event,jeu.getTheme());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void menuSubRejoindrePartie(Jeu jeu) {
		try {
			this.controlleurFen.ChangementFxml(this.controlleurFen,"MenuMultiJoin.fxml",event,jeu.getTheme());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void menuLobby(Jeu jeu) {
		try {
			this.controlleurFen.ChangementFxml(this.controlleurFen,"MenuMultiHost.fxml",event,jeu.getTheme());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void menuJoinCompCoop(Jeu jeu) {
		try {
			this.controlleurFen.ChangementFxml(this.controlleurFen,"MenuMultiHost.fxml",event,jeu.getTheme());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void menuServerName(Jeu Jeu) {
		try {
			this.controlleurFen.ChangementFxml(this.controlleurFen,"MenuMultiHost.fxml",event,Jeu.getTheme());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void menuPseudo(Jeu jeu) {
		Application.launch( com.mycompany.app.App.class);
	}

	@Override
	public void menuScoreboard(Jeu jeu) {

	}


	/**
	 * dans update, on met a jour les affichage des vue a partir des modeles contenu dans partie
	 */
	public void update() {
		//on update les modele
		if(this.modele.isModif()||this.modele.isCharge()){
			

			if(this.modele!=null) {
				
				display();
			}
			this.modele.setModif(false);
		}
	}

	/**
	 * Permet la gestion de l'affichage de la fin de partie
	 * @param gagne
	 * 	Vrai si on a gagné la partie, faux sinon.
	 */
	@Override
	public void finDePartie(boolean gagne) {
		/*
		to do
		Gestion de l'affichage de la fin de partie
		 */
	}


	public void display() {
		
		//si on a un enfant, on l'enleve, c'est l'ancien affichage
		this.getChildren().clear();
		
		//on ajoute la nouvelle version graphique 
		affichageGrille();
	
	
		/* Pour la mise a jour du score */
		if(this.getScene()!=null) {

			BorderPane p=(BorderPane) this.getScene().getRoot().getChildrenUnmodifiable().get(0);
			
			BorderPane p2=(BorderPane) p.getChildren().get(0);
			
			Pane p3=(Pane) p2.getChildren().get(4);
			
			Pane p4=(Pane) p3.getChildren().get(1);
			
			
			Label l =(Label)p4.getChildren().get(1);
			l.setText(String.valueOf(this.modele.getJoueur().getScore()));
		}
		
		/* Pour la mise a jour du nombre de déplacement */
		if(this.getScene()!=null) {

			BorderPane p=(BorderPane) this.getScene().getRoot().getChildrenUnmodifiable().get(0);
	
			BorderPane p2=(BorderPane) p.getChildren().get(0);
			
			Pane p3=(Pane) p2.getChildren().get(4);
			
			Pane p4=(Pane) p3.getChildren().get(2);
			
			
			Label l =(Label)p4.getChildren().get(1);
			l.setText(String.valueOf(this.modele.getNbTours()));
		}
		
		/* Pour la mise a jour du meilleur score */
		if(this.getScene()!=null) {

			BorderPane p=(BorderPane) this.getScene().getRoot().getChildrenUnmodifiable().get(0);
	
			BorderPane p2=(BorderPane) p.getChildren().get(0);
			
			Pane p3=(Pane) p2.getChildren().get(4);
			
			Pane p4=(Pane) p3.getChildren().get(0);
			
			
			Label l =(Label)p4.getChildren().get(1);
			
			ObjectInputStream ois = null;
			try {
				final FileInputStream fichierIn = new FileInputStream("score.ser");
				ois = new ObjectInputStream(fichierIn);
				try {
					int mscore = (int) ois.readObject();
					String scoreString = Integer.toString(mscore);
					l.setText(scoreString);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (IOException e) {
				//e.printStackTrace();
				System.out.println("Fichier introuvable");
			}
		}
		
		/*for(Node n:this.getScene().getRoot().getChildrenUnmodifiable()){
		System.out.println("test : "+n);
		
		//score*/
	}
	

	
	/**
	 * methode qui créer une grille en fonction des parametres entrés
	 * @param
	 * @return GraphicsContext qui contient une grille en plus
	 */
	public void affichageGrille() {
		//BorderPane retour = new BorderPane();
		
		//nombre de grille a dessiner
		int nbGrille=this.modele.getGrilles().size();
		//nombre de case d'une grille
		int nbCase=this.modele.getGrilles().get(0).getCases().length;
		
		//interstice d'une grille
		double decoupeGrille=nbCase*2+1;
		
		//position initiale de x (c'est le max divisé par le nombre de morceau d'une grille * le nombre de grille)
		double posInitX=this.maxX/(decoupeGrille*nbGrille);
		//position initiale de y, c'est le max y divisé par le nombre de morceaux d'une grille le tout divisé par 2 (pour par etre trop grand)
		double posInitY=(this.maxY/decoupeGrille)/2;
		
		//la taille de x c'est la taille max x diviser par le nombre d'interstice fois le nombre de grille 
		double tailleX=this.maxX/((nbCase+1)*nbGrille);
		//la taille de y c'est la taille max y diviser par le nombre de d'interstice (nombre de case +1)
		double tailleY=this.maxX/((nbCase+1)*nbGrille);
		
		
		
		
		/*------------------------Dessin-----------------------*/
		/*On va pour toutes les grilles, analyser toutes les cases en largeur et hauteur*/
		for(int grille=0;grille<nbGrille;grille++) {
			//Grille g=this.modele.getGrilles().get(grille);
			//Case[][] tabCase=g.getCases();
			
			for(int i=0;i<nbCase;i++) {
				for(int j=0;j<nbCase;j++) {
					//double x= /*partie espace entre les grilles*/(this.maxX/decoupeGrille*nbGrille*grille)  /*partie espace entre case*/+tailleX*i+posInitX*i;		
					double x=posInitX+(tailleX*i)+(this.maxX/nbGrille)*grille;
					//la position initiale + la taille fois la case courante y plus (la position initale fois la case Courante y diviser par 2)
				    double y=posInitY+tailleY*j;
				    
				    //creation des objet qui seront reuni (pane = fond, rectange = couleur, text = text)
				    Pane p = new Pane();
				    Rectangle r;
				    
				    /*création des réctangle de fond*/
				    r=creeRect(x,y,tailleX,tailleY,Color.BLACK,0);
			        this.getChildren().add(p);
					p.getChildren().add(r);
			        
			        r.setVisible(true);
			        p.setVisible(true);
				}
			}
		}
		
		/*----------------Dessin des tuile et utilisation du thread en fonction de la direction--------------------------------*/

		for(int grille=0;grille<nbGrille;grille++) {
			try {
				Grille g=this.modele.getGrilles().get(grille);
				Case[][] tabCase=g.getCases();for(int i=0;i<nbCase;i++) {
					for(int j=0;j<nbCase;j++) {
						Case caseCourante=tabCase[i][j];
						if(caseCourante.getClass().equals(CaseTuile.class)) 
							tabCase[i][j]=deplacementEtThread(((CaseTuile)caseCourante),i,j,nbCase,grille,posInitX,tailleX,posInitY,tailleY,nbGrille) ;
					}
				}
				/*if(this.modele.getDirectionCourante()==GestionJeux.DROITE) {
					for(int i=0;i<nbCase;i++) {
						for(int j=0;j<nbCase;j++) {
							Case caseCourante=tabCase[i][j];
							if(caseCourante.getClass().equals(CaseTuile.class)) 
								tabCase[i][j]=deplacementEtThread(((CaseTuile)caseCourante),i,j,nbCase,grille,posInitX,tailleX,posInitY,tailleY,nbGrille) ;
						}
					}
				}else if(this.modele.getDirectionCourante()==GestionJeux.GAUCHE) {
					for(int i=nbCase-1;i>=0;i--) {
						for(int j=0;j<nbCase;j++) {
							Case caseCourante=tabCase[i][j];
							if(caseCourante.getClass().equals(CaseTuile.class)) 
								tabCase[i][j]=deplacementEtThread(((CaseTuile)caseCourante),i,j,nbCase,grille,posInitX,tailleX,posInitY,tailleY,nbGrille) ;
						}
					}
				}else if(this.modele.getDirectionCourante()==GestionJeux.HAUT) {
					for(int j=0;j<nbCase;j++) {
						for(int i=0;i<nbCase;i++) {
								Case caseCourante=tabCase[i][j];
							if(caseCourante.getClass().equals(CaseTuile.class)) 
								tabCase[i][j]=deplacementEtThread(((CaseTuile)caseCourante),i,j,nbCase,grille,posInitX,tailleX,posInitY,tailleY,nbGrille) ;
						}
					}
				}else if(this.modele.getDirectionCourante()==GestionJeux.BAS) {
					for(int j=nbCase-1;j>=0;j--) {
						for(int i=0;i<nbCase;i++) {
								Case caseCourante=tabCase[i][j];
							if(caseCourante.getClass().equals(CaseTuile.class)) 
								tabCase[i][j]=deplacementEtThread(((CaseTuile)caseCourante),i,j,nbCase,grille,posInitX,tailleX,posInitY,tailleY,nbGrille) ;
						}
					}
				}else {
					for(int i=0;i<nbCase;i++) {
						for(int j=0;j<nbCase;j++) {
							Case caseCourante=tabCase[i][j];
							if(caseCourante.getClass().equals(CaseTuile.class)) 
								tabCase[i][j]=deplacementEtThread(((CaseTuile)caseCourante),i,j,nbCase,grille,posInitX,tailleX,posInitY,tailleY,nbGrille) ;
						}
					}
				}*/

			}catch(IndexOutOfBoundsException e) {
				e.getStackTrace();
			}
		}
		this.modele.setModif(false);
		//return retour;
	}
	
	 
	/**
	 * method qui va afficher la case ainsi que sont déplacement par un thrad si besoin
	 * @param caseCourante
	 * @param i
	 * @param j
	 * @param nbCase
	 * @param grille
	 * @param posInitX
	 * @param tailleX
	 * @param posInitY
	 * @param tailleY
	 * @param nbGrille
	 * @return
	 */
	public Case deplacementEtThread(CaseTuile caseCourante,int i,int j,int nbCase,int grille,double posInitX,double tailleX,double posInitY,double tailleY,int nbGrille) {
		Pane pTuile=new Pane();
        
        //double x= /*partie espace entre les grilles*/(this.maxX/decoupeGrille*nbGrille*grille)  /*partie espace entre case*/+tailleX*i+posInitX*i;		
		double xCourant=posInitX+(tailleX*j)+(this.maxX/nbGrille)*grille;
		//la position initiale + la taille fois la case courante y plus (la position initale fois la case Courante y diviser par 2)
	    double yCourant=posInitY+tailleY*i;

        if(caseCourante.getXAffichage()<0 || caseCourante.getYAffichage()<0) {
        	caseCourante.setPositionAffichage(i, j);
        }

        double xPrec=posInitX+(tailleX*((CaseTuile)caseCourante).getYAffichage())+(this.maxX/nbGrille)*grille;
    	double yPrec=posInitY+(tailleY*((CaseTuile)caseCourante).getXAffichage());
        
    	Text t=new Text();
    	
    	if(i!=((CaseTuile)caseCourante).getXAffichage() || j!=((CaseTuile)caseCourante).getYAffichage() ) {
	    	organiserPanel(pTuile,t,tailleX,tailleY,((CaseTuile)caseCourante),xPrec,yPrec);
	    	pTuile.setVisible(true);
	    	
		    ((CaseTuile)caseCourante).setPositionAffichage(i,j);
		    
		    System.out.println(this.getChildren().get(1));
		    
		   
		    Task<Void> task=new Task<Void>() {
		    	
	    		@Override protected Void call() throws Exception {
	    			final double distanceX=(pTuile.getLayoutX()-xCourant)/TEMPSDEPLACEMENT;
	    			final double distanceY=(pTuile.getLayoutY()-yCourant)/TEMPSDEPLACEMENT;

	    			for(int i=TEMPSDEPLACEMENT;i>=0;i--) {
	    				if (isCancelled()) {
	    	                updateMessage("Cancelled");
	    	                break;
	    	            }
	    				Platform.runLater(()->modele.setDemandeDeplacement(false));
	    				final int iterator=i;
	    				Platform.runLater(() ->pTuile.setLayoutX(xCourant+(distanceX*iterator)));
	    				Platform.runLater(() ->pTuile.setLayoutY(yCourant+(distanceY*iterator)));
						
	    				try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							if (isCancelled()) {
			                    //updateMessage("Cancelled");
			                    break;
			                }
							//e.printStackTrace();
						}
	    			}
	    			Platform.runLater(()->pTuile.relocate(xCourant, yCourant));
	    			Platform.runLater(()->modele.setDemandeDeplacement(true));
					return null;
	    		}
	    	};
	    	
	    	
	    	
	    	Thread.UncaughtExceptionHandler h = new Thread.UncaughtExceptionHandler() {
		        public void uncaughtException(Thread th, Throwable ex) {
		            System.out.println("Uncaught exception: " + ex);
		        }
		    };
	
		    
		    try {
		
		    	Thread th=new Thread(task);
		    	th.setUncaughtExceptionHandler(h);
		    	th.setDaemon(true); // le Thread s'exécutera en arrière-plan (démon informatique)
	        	th.start(); 
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }else {
	    	organiserPanel(pTuile,t,tailleX,tailleY,((CaseTuile)caseCourante),xCourant,yCourant);
	    	pTuile.setVisible(true);

	    	if(((CaseTuile)caseCourante).getNouveau()) {
		    	pTuile.setVisible(false);
				Task<Void> task=new Task<Void>() {
					@Override protected Void call() throws Exception {
						try {
							Thread.sleep(50*TEMPSDEPLACEMENT+100);
						} catch (InterruptedException e) {
							//e.printStackTrace();
						}
						Platform.runLater(() ->pTuile.setVisible(true));
						return null;
					}
				};
	      		try {
						
				    	Thread th=new Thread(task);
				    	th.setDaemon(true); // le Thread s'exécutera en arrière-plan (démon informatique)
			        	th.start(); 
					} catch (Exception e) {
						e.printStackTrace();
					}
		    }
	    	
	    	
		    ((CaseTuile)caseCourante).setPositionAffichage(i,j);
	    
	    }
	    
	    
	    
	    
	    return caseCourante;
	}

	
	/**
	 * method qui créer le pane qu'on affiche
	 * @param t
	 * @param tailleX
	 * @param tailleY
	 * @param caseTuile
	 * @param xPrec
	 * @param yPrec
	 * @return
	 */
	private void organiserPanel(Pane pTuile,Text t, double tailleX, double tailleY, CaseTuile caseTuile, double xPrec,double yPrec) {
		
		creerText(t,tailleX,tailleY,String.valueOf(caseTuile.getNombre()));

    	Rectangle rectangle = new Rectangle();
		rectangle.setWidth(tailleX);
		rectangle.setHeight(tailleY);
        rectangle.getStyleClass().add(new String("rectangle"+Integer.parseInt(t.getText())));
        
        this.getChildren().add(pTuile);
        
    	pTuile.setLayoutX(xPrec);
    	pTuile.setLayoutY(yPrec);
    	
    	pTuile.getChildren().add(rectangle);
		pTuile.getChildren().add(t);
		t.setLayoutY(tailleY);
		
	}

	/**
	 * methode qui utilise les parametres en entrée pour créer un element text qui va ce position en fonction de la taille du rectangle, avec taille et contenant le bon chiffre
	 * @param tailleX
	 * @param tailleY
	 * @return
	 */
	public void creerText(Text t,double tailleX,double tailleY,String text) {
		
		t.setText(text);
		t.prefWidth(tailleX);
		t.prefHeight(tailleY);
		
		//calcul de la taille du font
		double taille=tailleX/t.getBoundsInLocal().getWidth();
		if(taille>t.getBoundsInLocal().getWidth())taille=t.getBoundsInLocal().getWidth();
		
		Font font = Font.font(null,FontWeight.BOLD,taille*10);
		t.setFont(font);
		
		//calcul de la position du text
		t.setX(taille);
		t.setY(-((1-(t.getBoundsInLocal().getWidth()/tailleY))*tailleY)/2);
		
    
	    t.getStyleClass().add(new String("text"));
		t.setId("caseTexte");
		
	}
	
	/**
	 * classe qui va retourner un objet rectangle formé pour la couleur et qui sera disposé
	 * @param x
	 * @param y
	 * @param longeur
	 * @param hauteur
	 * @param c
	 * @return
	 */
	public Rectangle creeRect(double x,double y, double longeur, double hauteur,Color c,double nombre){
    
		Rectangle rectangle = new Rectangle();
		rectangle.setLayoutX(x);
		rectangle.setLayoutY(y);
		rectangle.setWidth(longeur);
		rectangle.setHeight(hauteur);
        rectangle.getStyleClass().add(new String("rectangle"+(int)nombre));
	
        return rectangle;
	}
	
	
	/**
	 * setteur des taille maximam du Group inital dans le fichier fxml, cette methode est appelé lors de l'implementation de la vue dans la scene ou lors du changement de taille dans les options
	 * @param layoutX
	 * @param layoutY
	 */
	public void setTailleMax(double layoutX, double layoutY) {
		this.maxX=layoutX;
		this.maxY=layoutY;
		
	}

}
