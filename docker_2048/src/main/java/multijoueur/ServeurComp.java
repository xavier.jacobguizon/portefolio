package multijoueur;

import model.Partie;

public class ServeurComp extends Serveur {

    public ServeurComp(String nom){

        super(nom);
        this.setGamemode("COMP");
    }

    /**
     * Permet de traîter la réception d'une partie dans le cas d'un jeu de type compétitif
     * @param idConnexion
     * @param partie
     */
    @Override
    public void ajouterUnePartie(String idConnexion, Partie partie) {
        /*
        1. Envoyer la partie à l'autre joueur
         */
        if(getNbJoueur() != 1){
            String idConnexionAutreJoueur = this.rechercheIDFromIDBlacklist(idConnexion);
            Connexion joueurAdverse = this.rechercheConnexionDepuisID(idConnexionAutreJoueur);
            joueurAdverse.envoiePartie(partie);
        }

    }

    /**
     * Permet de gérer la fin de partie lors d'un jeu multijoueur de type Compétitif
     * @param idConnexion
     * @param message
     */
    @Override
    public void gererFinDePartie(String idConnexion, Message message) {
        /*
        On notifie l'autre joueur qu'il à perdu ou gagné selon le message reçu par le joueur émetteur du message
         */
        Connexion autreJoueur = this.rechercheConnexionDepuisID(this.rechercheIDFromIDBlacklist(idConnexion));
        if(message.getMessage() == Message.END_WIN){
            //Le joueur émetteur à gagné
            autreJoueur.envoieFinDePartie(new Message(Message.END_LOSE));
        }else if(message.getMessage() == Message.END_LOSE){
            //Le joueur émetteur à perdu
            autreJoueur.envoieFinDePartie(new Message(Message.END_WIN));
        }
    }

}
