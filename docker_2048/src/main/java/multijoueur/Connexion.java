package multijoueur;

import model.Partie;

import java.io.*;
import java.net.Socket;
import java.util.UUID;

/**
 * Correspond à un joueur
 */
public class Connexion implements Runnable {

    private Socket socket;
    private ObjectInputStream input = null;
    private ObjectOutputStream output = null;
    private Serveur serveur;
    private String id;
    private boolean readyToPlay;

    public Connexion(Socket socket, Serveur serveur) {
        this.socket = socket;
        this.serveur = serveur;
        this.id = UUID.randomUUID().toString();
        try {
            this.output = new ObjectOutputStream(this.socket.getOutputStream());
            this.input = new ObjectInputStream(this.socket.getInputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (!this.socket.isClosed()) {
            try {
                Message message = (Message) this.input.readObject();
                if (message != null) {
                    switch (message.getMessage()) {
                        case Message.ENVOI_PARTIE:
                            //On recoit une partie
                            serveur.ajouterUnePartie(this.id, message.getObjet());
                            break;
                        case Message.ASK_PARTIE:
                            //On demande une partie (cas du coop)
                            Partie partie = serveur.getOhterPlayerPartie(this.id);
                            this.output.writeObject(new Message(Message.ENVOI_PARTIE, partie));
                            this.output.flush();
                            break;
                        case Message.ASK_NOMBRE_JOUEUR:
                            //On demande le nombre de joueur
                            this.output.writeObject(new Message(Message.ASW_NOMBRE_JOUEUR, this.serveur.getNbJoueur() + ""));
                            this.output.flush();
                            break;
                        case Message.READY_TO_PLAY:
                            //on signifie que l'on est prêt à jouer
                            this.readyToPlay = true;
                            break;
                        case Message.ASK_IF_READY_TO_PLAY:
                            //On souhaite savoir si tout le modne est prêt
                            boolean ready = this.serveur.allPlayerReady();
                            this.output.writeObject(new Message(Message.ASW_IF_READY_TO_PLAY, ready));
                            this.output.flush();
                            break;
                        case Message.END_LOSE:
                            case Message.END_WIN:
                            serveur.gererFinDePartie(this.id, message);
                            break;
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Permet d'envoyer une partie au client
     */
    public void envoiePartie(Partie partie) {
        try {
            this.output.writeObject(new Message(Message.ENVOI_PARTIE, partie));
            this.output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de notifier le joueur de la fin de la partie
     * @param message
     */
    public void envoieFinDePartie(Message message){
        try {
            this.output.writeObject(message);
            this.output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isReadyToPlay() {
        return readyToPlay;
    }

    public void setReadyToPlay(boolean readyToPlay) {
        this.readyToPlay = readyToPlay;
    }
}
