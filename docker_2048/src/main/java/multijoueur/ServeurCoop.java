package multijoueur;

import model.Partie;

public class ServeurCoop extends Serveur {

    public ServeurCoop(String nom){
        super(nom);
        this.setGamemode("COOP");
    }

    /**
     * Permet de traîter la réception d'une partie dans le cadre d'un serveur avec le type de jeu Coopératif
     * Dans le cas d'une partie Coop, lors de la réception d'une partie on la met à jour chez l'autre joueur
     * @param idConnexion
     * @param partie
     */
    @Override
    public void ajouterUnePartie(String idConnexion, Partie partie) {
        if(this.getNbJoueur() == 1){
            //cas où le joueur-hébergeur envoie sa partie
            this.getListesPartie().put(idConnexion,partie);
        }else{
            String idConnexionAutreJoueur = this.rechercheIDFromIDBlacklist(idConnexion);
            //On met à jour la partie chez le joueur qui vient de jouer
            this.getListesPartie().replace(idConnexion, partie);
            //On la met à jour chez l'autre joueur et on autorise le jeu
            this.getListesPartie().replace(idConnexionAutreJoueur, partie);
            this.getListesPartie().get(idConnexionAutreJoueur).switchAutorisationJouer();

            //On envoie la partie à l'autre joueur
            Connexion connexion = this.rechercheConnexionDepuisID(idConnexionAutreJoueur);
            connexion.envoiePartie(this.getListesPartie().get(idConnexionAutreJoueur));
        }


    }

    /**
     * Permet de gérer la fin de partie lors d'une partie COOP
     * @param idConnexion
     * @param message
     */
    @Override
    public void gererFinDePartie(String idConnexion, Message message) {
        Connexion autreJoueur = this.rechercheConnexionDepuisID(this.rechercheIDFromIDBlacklist(idConnexion));
        autreJoueur.envoieFinDePartie(message);
    }
}
