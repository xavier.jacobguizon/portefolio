package multijoueur;

import model.Partie;

import java.io.Serializable;

public class Message implements Serializable {
    public static final int ASK_NOMBRE_JOUEUR = 0;
    public static final int ASK_PARTIE = 3;
    public static final int READY_TO_PLAY = 4;
    public static final int ASK_IF_READY_TO_PLAY = 5;
    public static final int ASW_IF_READY_TO_PLAY = 6;
    public static final int ASW_NOMBRE_JOUEUR = 2;
    public static final int ENVOI_PARTIE = 1;
    public static final int END_WIN = 7;
    public static final int END_LOSE = 8;

    private int message;
    private Partie objet = null;
    private String reponse;
    private boolean reponseBool;

    public Message(int message){
        this.message = message;
    }

    public Message(int message, Partie partie){
        this.message = message;
        this.objet = partie;
    }

    public Message(int message, boolean reponse){
        this.message = message;
        this.reponseBool = reponse;
    }

    public Message(int message, String reponse){
        this.message = message;
        this.reponse = reponse;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public Partie getObjet() {
        return objet;
    }

    public void setObjet(Partie objet) {
        this.objet = objet;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public boolean isReponseBool() {
        return reponseBool;
    }

    public void setReponseBool(boolean reponseBool) {
        this.reponseBool = reponseBool;
    }
}
