package multijoueur;


import bdd.Serveurs;
import model.Partie;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Différent mode de jeu
 * <p>
 * Compétitif : Deux grilles, chacun sa grille mais on envoie et recoit
 */

/**
 * Lancement serveur et connexion auto
 * JoueurDB rejoins
 */


public abstract class Serveur implements Runnable {
    /**
     * Le port utilisé par le serveur
     */
    private int port = 8679;

    /**
     * Le serveurSocket permettant aux client de se connecter
     */
    private ServerSocket server;

    /**
     * Le nombre de joueur connecté
     */
    private int nbJoueur;

    /**
     * L'adresse IP du serveur (utilisé pour rejoindre ce dernier)
     */
    private String host;

    /**
     * Nom du serveur
     */
    private String nom;

    /**
     * Type de jeu du serveur "COMP" ou "COOP"
     */
    private String gamemode;

    /**
     * Hashmap liant l'identifiant de connexion à une partie
     */
    private HashMap<String, Partie> listesPartie;

    /**
     * liste de l'ensemble des Connexion.class des joueurs connecté
     */
    private ArrayList<Connexion> listesJoueurs;

    //Instance sur la BDD
    private Serveurs serveursBDD;


    /**
     * Permet la création d'une instance de serveur
     * @param nom
     */
    public Serveur(String nom) {
        try {
            this.nbJoueur = 0;
            this.nom = nom;
            this.listesPartie = new HashMap<String, Partie>();
            this.listesJoueurs = new ArrayList<Connexion>();
            this.server = new ServerSocket(this.port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lance un thread permettant la gestion de la connexion au serveur
     */
    @Override
    public void run() {
        try {
            while (true) {
                Socket socket = this.server.accept();
                this.nbJoueur++;
                Connexion client = new Connexion(socket, this);

                //S'il n'y qu'un joueur alors le joueur qui se connecte est l'hote
                if (this.nbJoueur == 1) {
                    this.host = InetAddress.getLocalHost().getHostAddress();
                    this.serveursBDD = new Serveurs(this.nom, "WAITING", 1, this.host, this.port, this.gamemode);
                    serveursBDD.save();
                } else {
                    serveursBDD.setNbJoueur(this.getNbJoueur());
                    serveursBDD.save();
                }
                this.listesJoueurs.add(client);
                this.listesPartie.put(client.getId(),null);
                Thread processus_connexion = new Thread(client);
                processus_connexion.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Permet de savoir si la partie peut démarrer
     *
     * @return
     */
    public boolean allPlayerReady() {
        boolean res = true;
        if (this.nbJoueur == 2) {
            for (Connexion connexion : this.listesJoueurs) {
                if (!connexion.isReadyToPlay()) {
                    res = false;
                }
            }
        } else {
            res = false;
        }

        if (res) {
            this.serveursBDD.setStatus("INGAME");
            this.serveursBDD.save();
        }

        return res;
    }

    /**
     * Permet de récupérer la partie de l'autre joueur
     *
     * @param idConnexion ID de connexion à exlure de la recherche (provient de celui qui demande)
     * @return
     */
    public Partie getOhterPlayerPartie(String idConnexion) {
        String idAutreJoueur = this.rechercheIDFromIDBlacklist(idConnexion);
        Partie partie = this.listesPartie.get(idAutreJoueur);
        return partie;
    }

    /**
     * Permet d'ajouter ou de mettre à jour une partie
     *
     * @param idConnexion
     * @param partie
     */
    public abstract void ajouterUnePartie(String idConnexion, Partie partie);

    /**
     * Permet la gestion de la fin de partie entre les joueurs
     * @param idConnexion
     * @param message
     */
    public abstract void gererFinDePartie(String idConnexion, Message message);

    /**
     * Permet de trouver l'ID de l'autre joueur
     *
     * @param idConnexionBlackList
     * @return
     */
    public String rechercheIDFromIDBlacklist(String idConnexionBlackList) {
        String idRecherche = "";
        for (String id : this.listesPartie.keySet()) {
            if (!id.equals(idConnexionBlackList)) {
                idRecherche = id;
            }
        }
        return idRecherche;
    }

    /**
     * Permet de récupérer une COnnexion depuis un identifiant
     *
     * @param idConnexion
     * @return
     */
    public Connexion rechercheConnexionDepuisID(String idConnexion) {

        Connexion res = null;

        for (int i = 0; i < this.listesJoueurs.size(); i++) {
            Connexion connexion = this.listesJoueurs.get(i);
            if (connexion.getId().equals(idConnexion)) {
                res = connexion;
            }
        }
        return res;
    }

    /**
     * Permet d'arreter le serveur
     */
    public void arreterLeServeur(){
        try {
            this.server.close();
            this.serveursBDD.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int getNbJoueur() {
        return nbJoueur;
    }

    public void setNbJoueur(int nbJoueur) {
        this.nbJoueur = nbJoueur;
    }

    public HashMap<String, Partie> getListesPartie() {
        return listesPartie;
    }

    public void setListesPartie(HashMap<String, Partie> listesPartie) {
        this.listesPartie = listesPartie;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public ServerSocket getServer() {
        return server;
    }

    public void setServer(ServerSocket server) {
        this.server = server;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public ArrayList<Connexion> getListesJoueurs() {
        return listesJoueurs;
    }

    public void setListesJoueurs(ArrayList<Connexion> listesJoueurs) {
        this.listesJoueurs = listesJoueurs;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getGamemode() {
        return gamemode;
    }

    public void setGamemode(String gamemode) {
        this.gamemode = gamemode;
    }

    public Serveurs getServeursBDD() {
        return serveursBDD;
    }

    public void setServeursBDD(Serveurs serveursBDD) {
        this.serveursBDD = serveursBDD;
    }
}
