package multijoueur;

import model.Partie;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements Runnable {

    private Socket socket;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private Partie partieRecu;
    private int nbJoueur;
    private boolean isServerReady;

    /**
     * Sujet du dernier message reçu
     */
    private int sujetDernierMessage;
    private boolean nouveauMessage;

    public Client(String host, int port){
        try{
            System.out.println("HOst : "+host+" Port : "+port);
            this.socket = new Socket(host, port);
            this.output = new ObjectOutputStream(this.socket.getOutputStream());
            this.input = new ObjectInputStream(this.socket.getInputStream());
            this.nouveauMessage = false;
            this.sujetDernierMessage= -1;
            this.isServerReady = false;
            this.nbJoueur = 0;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try{
            while(true){
                Message message = (Message)this.input.readObject();
                if(message != null){
                    this.nouveauMessage = true;
                    this.sujetDernierMessage = message.getMessage();
                    switch(message.getMessage()){
                        case Message.ASW_NOMBRE_JOUEUR:
                            this.nbJoueur = Integer.parseInt(message.getReponse());
                            break;
                        case Message.ENVOI_PARTIE:
                            //Reception d'une partie
                            this.partieRecu = message.getObjet();
                            break;
                        case Message.ASW_IF_READY_TO_PLAY:
                            this.isServerReady = message.isReponseBool();
                            break;
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * Permet d'envoyer la fin de partie aux autres joueurs
     * @param gagne
     */
    public void envoyerFinDePartie(boolean gagne){
        try{
            Message message = null;
            if(gagne){
                message = new Message(Message.END_WIN);
            }else{
                message = new Message(Message.END_LOSE);
            }
            this.output.writeObject(message);
            this.output.flush();
            this.output.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de savoir si tout le monde est prêt et si le jeu peut démarrer
     */
    public void demanderSiPret(){
        try{
            Message message = new Message(Message.ASK_IF_READY_TO_PLAY);
            this.output.writeObject(message);
            this.output.flush();
            this.output.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de notifier au serveur que l'on est prêt
     */
    public void setReady(){
        try{
            Message message = new Message(Message.READY_TO_PLAY);
            this.output.writeObject(message);
            this.output.flush();
            this.output.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de demander au Serveur le nombre de joueur connecté
     */
    public void demanderLeNombreDeJoueur(){
        try{
            Message message = new Message(Message.ASK_NOMBRE_JOUEUR);
            this.output.writeObject(message);
            this.output.flush();
            this.output.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de demander la partie de l'autre joueur au serveur
     */
    public void demanderPartieAutreJoueur(){
        try{
            Message message = new Message(Message.ASK_PARTIE);
            this.output.writeObject(message);
            this.output.flush();
            this.output.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet d'envoyer sa partie au serveur
     */
    public void envoyerPartie(Partie partie){
        try{
            Message message = new Message(Message.ENVOI_PARTIE, partie);
            this.output.writeObject(message);
            this.output.flush();
            this.output.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Partie getPartieRecu() {
        return partieRecu;
    }

    public void setPartieRecu(Partie partieRecu) {
        this.partieRecu = partieRecu;
    }

    public int getNbJoueur() {
        return nbJoueur;
    }

    public void setNbJoueur(int nbJoueur) {
        this.nbJoueur = nbJoueur;
    }

    public boolean isNouveauMessage() {
        return nouveauMessage;
    }

    public void setNouveauMessage(boolean nouveauMessage) {
        this.nouveauMessage = nouveauMessage;
    }

    public int getSujetDernierMessage() {
        return sujetDernierMessage;
    }

    public void setSujetDernierMessage(int sujetDernierMessage) {
        this.sujetDernierMessage = sujetDernierMessage;
    }

    public boolean isServerReady() {
        return isServerReady;
    }

    public void setServerReady(boolean serverReady) {
        isServerReady = serverReady;
    }
}
