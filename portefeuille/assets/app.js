/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

const $ = require('jquery');

// create global $ and jQuery variables
global.$ = global.jQuery = $;


require('bootstrap');

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// You can specify which plugins you need
import { Tooltip, Toast, Popover } from 'bootstrap';

// start the Stimulus application
import './bootstrap';

$(document).ready(function() {
  $('[data-toggle="popover"]').popover();

  // let box = document.querySelector('.box');

  // box.addEventListener('click', function() {
  //   $('#horizontal_nav').get(0).classList.toggle('move');
  // });

  // Gestion de la position de l'utilisateur par rapport a la page.
  gestionScroll();

  // Gestion de la navbarre 
  gestionNav();

  // // Detect scroll events
  window.onscroll = function() {
    gestionNav();
  };
});

/**
 * Fonction qui gère le scroll de l'utilisateur vers la section correspondant au lien scrollto selectionné dans la navbarre.
 * Cette fonction permet d'éviter la redirection, comme #home par exemple.
 */
function gestionScroll() {
  $('.scrollto').on("click", function(event) {
    event.preventDefault();

    var target = $(this).attr('href');

    $('html, body').animate({
      scrollTop: $(target).offset().top
    }, 500);
  });
}

/**
 * Fonction qui permet la gestion des attributs de la navbarre
 * Cette fonction va modifier les classe et attributs de header_navigation, navbarre, contenu_navbarre et support.
 * Cette modification va permettre, si l'utilisateur est en haut de la page en dessous de 40px, d'afficher la navbarre a l'horizontal fixé au top.
 * a l'inverse, la navbarre sera affiché sur le coté sous forme de boutons a deplier.
 */
function gestionNav() {
   if (document.documentElement.scrollTop < 40) {
    if(!$("#contenu_navbarre").hasClass("collapse")){
      
      $("#support").css("height", 0);

      // Modification du header pour permettre la position pour la mise en place vertical ou horizontal
      $("#header_navigation").toggleClass("d-flex flex-column justify-content-center position-fixed top-0 bottom-0 start-0 p-4");

      // Modification pour permettre la mise en place vertical ou horizontal (cette classe permet l'installation du navbar au top)
      $("#navbarre").toggleClass("nav-menu navbar-expand-lg navbar-light bg-light");

      // Modification pour cacher le contenu
      $("#contenu_navbarre").toggleClass("collapse");


    }
  }
  else {
    if($("#contenu_navbarre").hasClass("collapse")){

      $("#support").css("height", 40);

      // Modification du header pour permettre la position pour la mise en place vertical ou horizontal
      $("#header_navigation").toggleClass("d-flex flex-column justify-content-center position-fixed top-0 bottom-0 start-0 p-4");

      // Modification pour permettre la mise en place vertical ou horizontal (cette classe permet l'installation du navbar au top)
      $("#navbarre").toggleClass("nav-menu navbar-expand-lg navbar-light bg-light");

      // Modification pour cacher le contenu
      $("#contenu_navbarre").toggleClass("collapse");
    }
  }
}
