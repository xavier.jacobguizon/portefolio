<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TelechargementController extends AbstractController
{
    #[Route('/telechargement', name: 'app_telechargement')]
    public function index(): Response
    {
        return $this->render('telechargement/index.html.twig', [
            'controller_name' => 'TelechargementController',
        ]);
    }

    #[Route('/telechargement/{file}', name: 'telecharger_fichier')]
    public function download($file) 
    {
        $response = new BinaryFileResponse('telechargements/'.$file);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$file.'"');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $file
        );

        return $response;
    }
}
