<?php

namespace App\Controller;

use App\Entity\Projets;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AcceuilController extends AbstractController
{
    #[Route('/acceuil', name: 'app_acceuil')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $repo = $doctrine->getRepository(Projets::class);
        $projets = $repo->findAll();

        dump($projets);

        return $this->render('acceuil/index.html.twig', [
            'controller_name' => 'AcceuilController',
           'projets' => $projets,
        ]);
    }
}
